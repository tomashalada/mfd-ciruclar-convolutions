import numpy as np

def check_symmetric( A, rtol = 1e-08, atol = 1e-12 ):
    return np.allclose( A, A.T, rtol = rtol, atol = atol )

def build_matrix_M( m11, m12, m21, m22 ):
    return np.array( [ [ m11, m12 ],
                       [ m21, m22 ] ] )

# Eq. 7 - Kernel function.
def phi( x, h ):
    z = abs( x ) / h
    if 0 <= z < 0.5:
        return 2.0 / 3.0  - 4.0 * z * z + 4.0 * z * z * z
    elif 0.5 <= z < 1:
        return 4.0 / 3.0 - 4 * z + 4 * z * z - 4.0 / 3.0 * z * z * z
    else:
        return 0
phi_vectorized = np.vectorize( phi )

# Eq. 69 - Corner partitions characteristic function
def chi_ci( x, x_c, corner ):
    if corner == 0:
        if x <= x_c:
            return 1
        else:
            return 0
    if corner == 1:
        if x > x_c:
            return 1
        else:
            return 0
chi_ci_vectorized = np.vectorize( chi_ci, excluded=[ "x_c, corner" ] )

# Monomial H1 with corner shift (introduced within the eq. 77 )
def func_H1( x ):
    return 1
func_H1_vectorized = np.vectorize( func_H1, otypes=[ np.float64 ] )

# Monomial H2 with corner shift (introduced within the eq. 77 )
def func_H2( x ):
    return x
func_H2_vectorized = np.vectorize( func_H2, otypes=[ np.float64 ] )

# Kernel functions with corner shift (introduced within the eq. 77 )
def func_PHI( x, h ):
    return phi_vectorized( x, h )
func_PHI_vectorized = np.vectorize( func_PHI, excluded=[ "h" ], otypes=[ np.float64 ] )
