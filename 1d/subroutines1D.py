import numpy as np
from functions1D import *
import print_and_debug1D
import matplotlib.pyplot as plt

def create_H_vects( CHI, X, cx, coef_a ):

    n_per_dim = CHI.shape[ 0 ]
    x_min = X[ 0 ]
    x_max = X[ -1 ]
    dx = X[ 1 ] - X[ 0 ]
    x_len = x_max - x_min + dx
    x_c = x_min + x_len / 2
    print( f"----> x_c: {x_c}, x_max: {x_max}" )
    # x_c 0 gives the same matrices as the neighborLoopVersion
    # x_c = 0

    H1 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    H2 = np.zeros( [ n_per_dim ], dtype=np.float64 )

    # Eq. 79 - adjusted monomial basis functions
    for corner in range( 0 , 2 ):
        print()
        # Variant A - directly reimplemented from the paper with x_c in chi_ci char function
        #H1 +=  chi_ci_vectorized( X, x_c, corner ) * func_H1_vectorized( X - cx[ corner ] )
        #H2 +=  chi_ci_vectorized( X, x_c, corner ) * func_H2_vectorized( X - cx[ corner ] )

        # Variant B - directly reimplemented from the paper with 0 in chi_ci char function
        #H1 +=  chi_ci_vectorized( X, 0, corner ) * func_H1_vectorized( X - cx[ corner ] )
        #H2 +=  chi_ci_vectorized( X, 0, corner ) * func_H2_vectorized( X - cx[ corner ] )

        # Variant C - use np.ff.fftshif with chi_ci characteristic function centered in x_c
        #H1 += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( X - x_c ) )
        #H2 += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( X - x_c ) )

        # Variant G - use np.ff.fftshif with chi_ci, shift kernel and vectors together
        #H1 += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( X - x_c ) )
        #H2 += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( X - x_c ) )

    # Variant D - use np.ff.fftshif only without characteristic function chi_ci
    H1 = np.fft.fftshift( func_H1_vectorized( X - x_c ) )
    H2 = np.fft.fftshift( func_H2_vectorized( X - x_c ) )

    ## Variant E - use np.ff.fftshif only without characteristic function chi_ci, centered at zero
    #H1 = np.fft.fftshift( func_H1_vectorized( X - 0 ) )
    #H2 = np.fft.fftshift( func_H2_vectorized( X - 0 ) )

    # Variant F - shift both together without chi_ci function
    #H1 = np.fft.fftshift( func_H1_vectorized( X - x_c ) )
    #H2 = np.fft.fftshift( func_H2_vectorized( X - x_c ) )

    return H1, H2

def create_Ha_vects( CHI, X, cx, coef_a ):

    n_per_dim = CHI.shape[ 0 ]
    x_min = X[ 0 ]
    x_max = X[ -1 ]
    dx = X[ 1 ] - X[ 0 ]
    x_len = x_max - x_min + dx
    x_c = x_min + x_len / 2
    # x_c 0 gives the same matrices as the neighborLoopVersion
    # x_c = 0

    H1a = np.zeros( [ n_per_dim ], dtype=np.float64 )
    H2a = np.zeros( [ n_per_dim ], dtype=np.float64 )

    # Eq. 78 - adjusted monomial basis functions
    for corner in range( 0 , 2 ):
        print()

        # Variant A - directly reimplemented from the paper with x_c in chi_ci char function
        #H1a +=  chi_ci_vectorized( X, x_c, corner ) * func_H1_vectorized( X - cx[ corner ] ) * func_PHI_vectorized( X - cx[ corner ], coef_a )
        #H2a +=  chi_ci_vectorized( X, x_c, corner ) * func_H2_vectorized( X - cx[ corner ] ) * func_PHI_vectorized( X - cx[ corner ], coef_a )

        # Variant B - directly reimplemented from the paper with 0 in chi_ci char function
        #H1a +=  chi_ci_vectorized( X, 0, corner ) * func_H1_vectorized( X - cx[ corner ] ) * func_PHI_vectorized( X - cx[ corner ], coef_a )
        #H2a +=  chi_ci_vectorized( X, 0, corner ) * func_H2_vectorized( X - cx[ corner ] ) * func_PHI_vectorized( X - cx[ corner ], coef_a )

        # Variant C - use np.ff.fftshif with chi_ci characteristic function centered in x_c
        #H1a += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( X - x_c ) ) * np.fft.fftshift( func_PHI_vectorized( X - x_c, coef_a ) )
        #H2a += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( X - x_c ) ) * np.fft.fftshift( func_PHI_vectorized( X - x_c, coef_a ) )

        #H1a +=  chi_ci_vectorized( X, x_c, corner ) * func_H1_vectorized( X - cx[ corner ] ) * np.fft.fftshift( func_PHI_vectorized( X - x_c, coef_a ) )
        #H2a +=  chi_ci_vectorized( X, x_c, corner ) * func_H2_vectorized( X - cx[ corner ] ) * np.fft.fftshift( func_PHI_vectorized( X - x_c, coef_a ) )

        # Variant G - use np.ff.fftshif with chi_ci, shift kernel and vectors together
        #H1a += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( X - x_c ) * func_PHI_vectorized( X - x_c, coef_a ) )
        #H2a += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( X - x_c ) * func_PHI_vectorized( X - x_c, coef_a ) )

    # Variant D - use np.ff.fftshif only without characteristic function chi_ci
    H1a = np.fft.fftshift( func_H1_vectorized( X - x_c ) ) * np.fft.fftshift( func_PHI_vectorized( X - x_c, coef_a ) )
    H2a = np.fft.fftshift( func_H2_vectorized( X - x_c ) ) * np.fft.fftshift( func_PHI_vectorized( X - x_c, coef_a ) )

    # Variant E - use np.ff.fftshif only without characteristic function chi_ci centered at zero
    #H1a = np.fft.fftshift( func_H1_vectorized( X - 0 ) ) * np.fft.fftshift( func_PHI_vectorized( X - 0, coef_a ) )
    #H2a = np.fft.fftshift( func_H2_vectorized( X - 0 ) ) * np.fft.fftshift( func_PHI_vectorized( X - 0, coef_a ) )

    # Variant F - shift both together without chi_ci function
    #H1a =  np.fft.fftshift( func_H1_vectorized( X - x_c ) * func_PHI_vectorized( X - x_c, coef_a ) )
    #H2a =  np.fft.fftshift( func_H2_vectorized( X - x_c ) * func_PHI_vectorized( X - x_c, coef_a ) )

    return H1a, H2a

def create_Ha_bar_vects( CHI, X, cx, coef_a ):

    n_per_dim = CHI.shape[ 0 ]
    x_min = X[ 0 ]
    x_max = X[ -1 ]
    dx = X[ 1 ] - X[ 0 ]
    x_len = x_max - x_min + dx
    x_c = x_min + x_len / 2
    # x_c 0 gives the same matrices as the neighborLoopVersion
    # x_c = 0

    H1a_bar = np.zeros( [ n_per_dim ], dtype=np.float64 )
    H2a_bar = np.zeros( [ n_per_dim ], dtype=np.float64 )

    # Eq. 79 - adjusted bar monomial basis functions
    for corner in range( 0 , 2 ):
        print()
        # Variant A - directly reimplemented from the paper with x_c i chi_ci char function
        #H1a_bar += chi_ci_vectorized( X, x_c, corner ) * func_H1_vectorized( ( -1 ) * ( X - cx[ corner ] ) ) * func_PHI_vectorized( ( -1 ) * ( X - cx[ corner ] ), coef_a )
        #H2a_bar += chi_ci_vectorized( X, x_c, corner ) * func_H2_vectorized( ( -1 ) * ( X - cx[ corner ] ) ) * func_PHI_vectorized( ( -1 ) * ( X - cx[ corner ] ), coef_a )

        # Variant B - directly reimplemented from the paper with 0 in chi_ci char function
        #H1a_bar += chi_ci_vectorized( X, 0, corner ) * func_H1_vectorized( ( -1 ) * ( X - cx[ corner ] ) ) * func_PHI_vectorized( ( -1 ) * ( X - cx[ corner ] ), coef_a )
        #H2a_bar += chi_ci_vectorized( X, 0, corner ) * func_H2_vectorized( ( -1 ) * ( X - cx[ corner ] ) ) * func_PHI_vectorized( ( -1 ) * ( X - cx[ corner ] ), coef_a )

        # Variant C - use np.ff.fftshif with chi_ci characteristic function centered in x_c
        #H1a_bar += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( ( -1 ) * ( X - x_c ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
        #H2a_bar += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( ( -1 ) * ( X - x_c ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )

        #H1a_bar += chi_ci_vectorized( X, x_c, corner ) * func_H1_vectorized( ( -1 ) * ( X - cx[ corner ] ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
        #H2a_bar += chi_ci_vectorized( X, x_c, corner ) * func_H2_vectorized( ( -1 ) * ( X - cx[ corner ] ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )

        # Variant G - use np.ff.fftshif with chi_ci, shift kernel and vectors together
        #H1a_bar += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( ( -1 ) * ( X - x_c ) ) * func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
        #H2a_bar += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( ( -1 ) * ( X - x_c ) ) * func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )

    # Variant D - use np.ff.fftshif only without characteristic function chi_ci
    H1a_bar = np.fft.fftshift( func_H1_vectorized( ( -1 ) * ( X - x_c ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
    H2a_bar = np.fft.fftshift( func_H2_vectorized( ( -1 ) * ( X - x_c ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )

    # Variant E - use np.ff.fftshif only without characteristic function chi_ci centered at zero
    #H1a_bar = np.fft.fftshift( func_H1_vectorized( ( -1 ) * ( X - 0 ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - 0 ), coef_a ) )
    #H2a_bar = np.fft.fftshift( func_H2_vectorized( ( -1 ) * ( X - 0 ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - 0 ), coef_a ) )

    # Variant F - shift both together without chi_ci function
    #H1a_bar = np.fft.fftshift( func_H1_vectorized( ( -1 ) * ( X - x_c ) ) * func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
    #H2a_bar = np.fft.fftshift( func_H2_vectorized( ( -1 ) * ( X - x_c ) ) * func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )

    return H1a_bar, H2a_bar

def eval_b_vects( CHI, H1, H2, H1a, H2a, X, coef_a ):

    # mapping vectors
    C0 = np.array( [ 1, 0 ] ).T
    Cdx = np.array( [ 0, -1 ] ).T

    n_per_dim = CHI.shape[ 0 ]
    CHI_hat = np.fft.fft( CHI )
    M11 = np.zeros( [ n_per_dim ] )
    M12 = np.zeros( [ n_per_dim ] )
    M21 = np.zeros( [ n_per_dim ] )
    M22 = np.zeros( [ n_per_dim ] )

    ### a) --- CIRUCLAR CONVOLUTIONS COMPUTED USING FFT ------
    M11 = ( 1 - CHI ) + CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H1 * H1a ) ) )
    M12 =               CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H1 * H2a ) ) )
    M21 =               CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H2 * H1a ) ) )
    M22 = ( 1 - CHI ) + CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H2 * H2a ) ) )
    # --- END ---

    ### b) CIRUCLAR CONVOLUTIONS COMPUTED WITHOUT FFT
    #b) for i in range( 0, n_per_dim ):

    #b)     m11_tmp = 0
    #b)     m12_tmp = 0
    #b)     m21_tmp = 0
    #b)     m22_tmp = 0
    #b)     for k in range( 0 , n_per_dim ):
    #b)         m11_tmp += CHI[ k ] * func_H1( X[ i ] - X[ k ] ) *  func_H1( X[ i ] - X[ k ] ) * phi( X[ i ] - X[ k ], coef_a )
    #b)         m12_tmp += CHI[ k ] * func_H1( X[ i ] - X[ k ] ) *  func_H2( X[ i ] - X[ k ] ) * phi( X[ i ] - X[ k ], coef_a )
    #b)         m21_tmp += CHI[ k ] * func_H2( X[ i ] - X[ k ] ) *  func_H1( X[ i ] - X[ k ] ) * phi( X[ i ] - X[ k ], coef_a )
    #b)         m22_tmp += CHI[ k ] * func_H2( X[ i ] - X[ k ] ) *  func_H2( X[ i ] - X[ k ] ) * phi( X[ i ] - X[ k ], coef_a )

    #b)     M11[ i ] = ( 1 -  CHI[ i ] ) + CHI[ i ] * m11_tmp
    #b)     M12[ i ] = CHI[ i ] * m12_tmp
    #b)     M21[ i ] = CHI[ i ] * m21_tmp
    #b)     M22[ i ] = ( 1 -  CHI[ i ] ) + CHI[ i ] * m22_tmp
    # --- END ---

    #print_and_debug1D.plot_precomputed_stuctures_M_matrices( X, M11, M12, M21, M22, False )
    #print_and_debug1D.plot_precomputed_stuctures_M_matrices_HvectorProducts( X, H1 * H1a, H1 * H2a, H2 * H1a, H2 * H2a, False )
    #print_and_debug1D.plot_precomputed_stuctures_M_matrices_ChiHatHpHq( X, CHI_hat * np.fft.fft( H1 * H1a ),
    #                                                                       CHI_hat * np.fft.fft( H1 * H2a ),
    #                                                                       CHI_hat * np.fft.fft( H2 * H1a ),
    #                                                                       CHI_hat * np.fft.fft( H2 * H2a ), False )

    #NOTE: There is a problem with the realization and numpy real and imaginary parts.
    b01 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    b02 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    bx1 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    bx2 = np.zeros( [ n_per_dim ], dtype=np.float64 )

    for i in range( 0, n_per_dim ):

        # Eq. 80 - Local momentum matrix
        M_local = build_matrix_M( M11[ i ], M12[ i ], M21[ i ], M22[ i ] )
        #print( np.linalg.cond( M_local ) )
        print( f"i: {i}, x[i]: {X[i]}, CHI: {CHI[ i ]}, cond(M): {np.linalg.cond( M_local )}, M_local:" )
        print( M_local )
        #if check_symmetric( M_local ) == False:
        if M12[ i ] != M21[ i ]:
            exit( "M_local is not symmetric!" )
        invM_local = np.linalg.inv( M_local )

        #TODO: fix this confusing indexing
        b01[ i ] = invM_local[ 0, 0 ]
        b02[ i ] = invM_local[ 0, 1 ]
        bx1[ i ] = ( -1.0 ) * invM_local[ 1, 0 ]
        bx2[ i ] = ( -1.0 ) * invM_local[ 1, 1 ]

    return b01, b02, bx1, bx2

def eval_f_int( d_input, bx1, bx2, CHI, V, H1a, H2a, H1a_bar, H2a_bar, X, coef_a ):

    n_per_dim = d_input.shape[ 0 ]
    d_hat = np.fft.fft( CHI * d_input )
    f_int = np.zeros( [ n_per_dim ] )

    ### 1) CIRUCLAR CONVOLUTIONS COMPUTED USING FFT
    Ax = np.zeros( [ n_per_dim ], dtype=np.float64 )

    # s dependent unrolled for
    D1 = np.real( np.fft.ifft( d_hat * np.fft.fft( H1a ) ) )
    D2 = np.real( np.fft.ifft( d_hat * np.fft.fft( H2a ) ) )
    Ax += bx1 * D1 + bx2 * D2

    B_hat = np.fft.fft( np.zeros( [ n_per_dim ], dtype=np.float64 ) )

    # s dependent unrolled for
    Cx1 = CHI * V * bx1
    Cx2 = CHI * V * bx2
    B_hat += ( np.fft.fft( Cx1 * Ax ) * np.fft.fft( H1a_bar ) )
    B_hat += ( np.fft.fft( Cx2 * Ax ) * np.fft.fft( H2a_bar ) )

    f_int = CHI * np.real( np.fft.ifft( B_hat ) )
    # --- END ---

    ### 2) CIRUCLAR CONVOLUTIONS COMPUTED WITHOUT FFT
    #2) for i in range( 0, n_per_dim ):

    #2)     fi_tmp = 0
    #2)     for s in range( 0 , n_per_dim ):

    #2)         ### a) Equation 54
    #2)         fi_H1_chi_d_temp_s = 0
    #2)         fi_H2_chi_d_temp_s = 0
    #2)         for j in range( 0, n_per_dim ):
    #2)             fi_H1_chi_d_temp_s += func_H1( X[ s ] - X[ j ] ) * phi( X[ s ] - X[ j ], coef_a ) * CHI[ j ] * d_input[ j ]
    #2)             fi_H2_chi_d_temp_s += func_H2( X[ s ] - X[ j ] ) * phi( X[ s ] - X[ j ], coef_a ) * CHI[ j ] * d_input[ j ]

    #2)         #fi_tmp += CHI[ s ] * CHI[ i ] * bx1[ s ] * func_H1( X[ s ] - X[ i ] ) * phi( X[ s ] - X[ i ], coef_a ) * bx1[ s ] * fi_H1_chi_d_temp_s * V[ s ]
    #2)         #fi_tmp += CHI[ s ] * CHI[ i ] * bx1[ s ] * func_H1( X[ s ] - X[ i ] ) * phi( X[ s ] - X[ i ], coef_a ) * bx2[ s ] * fi_H2_chi_d_temp_s * V[ s ]
    #2)         #fi_tmp += CHI[ s ] * CHI[ i ] * bx2[ s ] * func_H2( X[ s ] - X[ i ] ) * phi( X[ s ] - X[ i ], coef_a ) * bx1[ s ] * fi_H1_chi_d_temp_s * V[ s ]
    #2)         #fi_tmp += CHI[ s ] * CHI[ i ] * bx2[ s ] * func_H2( X[ s ] - X[ i ] ) * phi( X[ s ] - X[ i ], coef_a ) * bx2[ s ] * fi_H2_chi_d_temp_s * V[ s ]

    #2)         #NOTE: Remove CHI[ s ] actually works fine aswell
    #2)         #fi_tmp += CHI[ i ] * \
    #2)         fi_tmp += CHI[ s ] * CHI[ i ] * \
    #2)                 ( bx1[ s ] * func_H1( X[ s ] - X[ i ] ) * phi( X[ s ] - X[ i ], coef_a ) +  bx2[ s ] * func_H2( X[ s ] - X[ i ] ) * phi( X[ s ] - X[ i ], coef_a ) ) * \
    #2)                 ( bx1[ s ] * fi_H1_chi_d_temp_s + bx2[ s ] * fi_H2_chi_d_temp_s ) * V[ s ]

    #2)         ### b) Equation 51
    #2)         #b) j_loop_temp_var = 0
    #2)         #b) for j in range( 0, n_per_dim ):

    #2)         #b)     #NOTE: Remove CHI[ s ] also helps to solve this issue:
    #2)         #b)     #j_loop_temp_var += CHI[ s ] * CHI [ i ] * CHI[ j ] * \
    #2)         #b)     j_loop_temp_var += CHI [ i ] * CHI[ j ] * \
    #2)         #b)                     ( bx1[ s ] * func_H1( X[ s ] - X[ i ] ) * phi( X[ s ] - X[ i ], coef_a ) +  bx2[ s ] * func_H2( X[ s ] - X[ i ] ) * phi( X[ s ] - X[ i ], coef_a ) ) * \
    #2)         #b)                     ( bx1[ s ] * func_H1( X[ s ] - X[ j ] ) * phi( X[ s ] - X[ j ], coef_a ) +  bx2[ s ] * func_H2( X[ s ] - X[ j ] ) * phi( X[ s ] - X[ j ], coef_a ) ) * \
    #2)         #b)                     d_input [ j ]

    #2)         #b) fi_tmp += j_loop_temp_var * V[ s ]


    #2)     f_int[ i ] = fi_tmp
    # --- END ---

    #: ### 3) NO CIRCULAR CONVOLUTIONS
    #: K = np.zeros( [ n_per_dim , n_per_dim ] )
    #: for i in range( 0, n_per_dim  ):
    #:     for j in range( 0, n_per_dim  ):

    #:         Kij = 0
    #:         for k in range( 0, n_per_dim ):
    #:             bx_vect = np.array( [ bx1[ k ], bx2[ k ] ] )
    #:             H_Xk_Xi = np.array( [ func_H1( X[ k ] - X[ i ] ), func_H2( X[ k ] - X[ i ] ) ] )
    #:             H_Xk_Xj = np.array( [ func_H1( X[ k ] - X[ j ] ), func_H2( X[ k ] - X[ j ] ) ] )

    #:             psi_i = np.dot( bx_vect, H_Xk_Xi ) * phi( X[ k ] - X[ i ], coef_a )
    #:             psi_j = np.dot( bx_vect, H_Xk_Xj ) * phi( X[ k ] - X[ j ], coef_a )
    #:             Kij += psi_i * psi_j * V[ k ]

    #:         K[ i, j ] = Kij

    #: np.set_printoptions( precision=2, suppress=True )
    #: print( f"Matrix K:\n{K}" )
    #: print( K )
    #: #plt.imshow( K )
    #: #plt.show()

    #: Kred = K[ 1 : n_per_dim - 2, 1 : n_per_dim - 2 ]
    #: print( f"Matrix Kred:\n{Kred}" )
    #: print( Kred )
    #: #plt.imshow(  Kred )
    #: #plt.show()

    #print("*******************************************************************************************************************")
    #print( "f_INT:" )
    #print( f_int )
    #print( "f_int_crop:" )
    #f_in_NOCC = np.zeros( [ n_per_dim ] )
    ##f_in_NOCC[ 1 : n_per_dim - 2 ] += Kred @ d_input[ 1 : n_per_dim - 2 ]
    #f_in_NOCC = K @ d_input
    #print( f_in_NOCC )
    #print("diff:")
    ##f_int_CC_NOCC_DIFF = np.copy( f_int )
    ##f_int_CC_NOCC_DIFF[ 1 : n_per_dim - 2 ] -= Kred @ d_input[ 1 : n_per_dim - 2 ]
    #f_int_CC_NOCC_DIFF = f_int - f_in_NOCC
    #print(f_int_CC_NOCC_DIFF)
    #print("*******************************************************************************************************************")

    return f_int

def eval_f_r( r, b01, b02, CHI, V, H1a_bar, H2a_bar, X, coef_a ):

    n_per_dim = r.shape[ 0 ]
    f_r = np.zeros( [ n_per_dim ] )

    # CIRUCLAR CONVOLUTIONS COMPUTED USING FFT
    B_hat = np.fft.fft( np.zeros( [ n_per_dim ], dtype=np.float64 ) )

    # s dependent unrolled for
    C01 = CHI * V * b01
    C02 = CHI * V * b02
    B_hat += ( np.fft.fft( C01 * r ) * np.fft.fft( H1a_bar ) )
    B_hat += ( np.fft.fft( C02 * r ) * np.fft.fft( H2a_bar ) )

    f_r = CHI * np.real( np.fft.ifft( B_hat ) )

    ## CIRUCLAR CONVOLUTIONS COMPUTED WITHOUT FFT
    #: for i in range( 0, n_per_dim ):

    #:     fri_tmp = 0
    #:     for s in range( 0 , n_per_dim ):
    #:         fri_tmp += CHI[ s ] * ( b01[ s ] * func_H1( X[ s ] - X[ i ] ) * phi( X[ s ] - X[ i ], coef_a ) + b02[ s ] * func_H2( X[ s ] - X[ i ] ) * phi( X[ s ] - X[ i ], coef_a ) ) * r[ s ] * V[ s ]

    #:     f_r[ i ] = CHI[ i ] * fri_tmp

    return f_r

def eval_field( d, b01, b02, CHI, H1a, H2a, x, coef_a ):

    n_per_dim = d.shape[ 0 ]

    d_hat = np.fft.fft( CHI * d )
    u_h = np.zeros( [ n_per_dim ], dtype=np.float64 )

    # s dependent unrolled for
    D1 = np.real( np.fft.ifft( d_hat * np.fft.fft( H1a ) ) )
    D2 = np.real( np.fft.ifft( d_hat * np.fft.fft( H2a ) ) )
    u_h += ( b01 * CHI ) * D1 + ( b02 * CHI ) * D2

    return u_h
