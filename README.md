## Resources
| date     | paper                                                                                                                                                                                 | contet                                                                                                                                |
| -------- | ------------------------------------------------------------------------------------------                                                                                            | ------------------------------------------------------------------------------------------------------------------------              |
| 2024 Mar | [An Ultra-high-speed Reproducing Kernel Particle Method](https://arxiv.org/abs/2403.19854)                                                                                            | Introduction of FC-RKPM method with algorithms, 1D, 2D and 3D examples and comparison with standard neighbor search RKPM              |
| 2023 Apr | [PeriFast/Corrosion: A 3D Pseudospectral Peridynamic MATLAB Code for Corrosion](https://link.springer.com/article/10.1007/s42102-023-00098-5)                                         | Brief description of the MATLAB code related to PeriFast/Corrosion, FCBM techniques explained, together with cool corrosion examples. |
| 2023 Mar | [PeriFast/Dynamics: A MATLAB Code for Explicit Fast Convolution-based Peridynamic Analysis of Deformation and Fracture](https://link.springer.com/article/10.1007/s42102-023-00097-6) | Brief description of the MATLAB code related to PeriFast/Dynamics, FCBM techniques explained, together with cool examples.            |
| 2022 Mar | [A general and fast convolution-based method for peridynamics: Applications to elasticity and brittle fracture](https://doi.org/10.1016/j.cma.2022.114666)                            | Introduction of FCBM for peridynamics.                                                                                                |

<!---
This is preprint of 2023 Apr I guess.
| 2022 Sep      | [PeriFast/Corrosion: a 3D pseudo-spectral peridynamic Matlab code for corrosion](https://www.researchsquare.com/article/rs-2046856/v1)        | Brief description of the MATLAB code related to PeriFast/Corrosion, FCBM techniques explained, together with cool corrosion examples. |
--->
