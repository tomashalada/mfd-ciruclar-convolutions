import numpy as np
from functions2D import *

def create_H_vects( CHI, X, Y, cx, cy, coef_a ):

    n_per_dim = CHI.shape[ 0 ]

    X_min = X[ 0, 0 ]
    X_max = X[ -1, -1 ]
    dx = X[ 0, 1 ] - X[ 0, 0 ]
    Y_min = Y[ 0, 0 ]
    Y_max = Y[ -1, -1 ]
    dy = Y[ 1, 0 ] - Y[ 0, 0 ]
    X_len = X_max - X_min + dx
    Y_len = Y_max - Y_min + dy
    X_c = X_min + X_len / 2
    Y_c = Y_min + Y_len / 2

    H1 = np.fft.fftshift( func_H1_vectorized( X - X_c, Y - Y_c ) )
    H2 = np.fft.fftshift( func_H2_vectorized( X - X_c, Y - Y_c ) )
    H3 = np.fft.fftshift( func_H3_vectorized( X - X_c, Y - Y_c ) )

    return H1, H2, H3

def create_Ha_vects( CHI, X, Y, cx, cy, coef_a ):

    n_per_dim = CHI.shape[ 0 ]

    X_min = X[ 0, 0 ]
    X_max = X[ -1, -1 ]
    dx = X[ 0, 1 ] - X[ 0, 0 ]
    Y_min = Y[ 0, 0 ]
    Y_max = Y[ -1, -1 ]
    dy = Y[ 1, 0 ] - Y[ 0, 0 ]
    X_len = X_max - X_min + dx
    Y_len = Y_max - Y_min + dy
    X_c = X_min + X_len / 2
    Y_c = Y_min + Y_len / 2

    H1a = np.fft.fftshift( func_H1_vectorized( X - X_c, Y - Y_c ) ) * np.fft.fftshift( func_PHI_vectorized( X - X_c, Y - Y_c, coef_a ) )
    H2a = np.fft.fftshift( func_H2_vectorized( X - X_c, Y - Y_c ) ) * np.fft.fftshift( func_PHI_vectorized( X - X_c, Y - Y_c, coef_a ) )
    H3a = np.fft.fftshift( func_H3_vectorized( X - X_c, Y - Y_c ) ) * np.fft.fftshift( func_PHI_vectorized( X - X_c, Y - Y_c, coef_a ) )

    return H1a, H2a, H3a

def create_Ha_bar_vects( CHI, X, Y, cx, cy, coef_a ):

    n_per_dim = CHI.shape[ 0 ]

    X_min = X[ 0, 0 ]
    X_max = X[ -1, -1 ]
    dx = X[ 0, 1 ] - X[ 0, 0 ]
    Y_min = Y[ 0, 0 ]
    Y_max = Y[ -1, -1 ]
    dy = Y[ 1, 0 ] - Y[ 0, 0 ]
    X_len = X_max - X_min + dx
    Y_len = Y_max - Y_min + dy
    X_c = X_min + X_len / 2
    Y_c = Y_min + Y_len / 2

    H1a_bar = np.fft.fftshift( func_H1_vectorized( ( -1 ) * ( X - X_c ), ( -1 ) * ( Y -  Y_c ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - X_c ), ( -1 ) * ( Y - Y_c ), coef_a ) )
    H2a_bar = np.fft.fftshift( func_H2_vectorized( ( -1 ) * ( X - X_c ), ( -1 ) * ( Y -  Y_c ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - X_c ), ( -1 ) * ( Y - Y_c ), coef_a ) )
    H3a_bar = np.fft.fftshift( func_H3_vectorized( ( -1 ) * ( X - X_c ), ( -1 ) * ( Y -  Y_c ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - X_c ), ( -1 ) * ( Y - Y_c ), coef_a ) )

    return H1a_bar, H2a_bar, H3a_bar

def eval_b_vects( CHI, H1, H2, H3, H1a, H2a, H3a ):

    n_per_dim = CHI.shape[ 0 ]

    n_per_dim = CHI.shape[ 0 ]
    CHI_hat = np.fft.fft2( CHI )
    M11 = np.zeros( [ n_per_dim ] )
    M12 = np.zeros( [ n_per_dim ] )
    M21 = np.zeros( [ n_per_dim ] )
    M22 = np.zeros( [ n_per_dim ] )

    # unrolled for p in [ 1, s ]
    # unrolled for q in [ p, s ]
    M11 = ( 1 - CHI ) + CHI * np.real( np.fft.ifft2( CHI_hat * np.fft.fft2( H1 * H1a ) ) )
    M12 =               CHI * np.real( np.fft.ifft2( CHI_hat * np.fft.fft2( H1 * H2a ) ) )
    M13 =               CHI * np.real( np.fft.ifft2( CHI_hat * np.fft.fft2( H1 * H3a ) ) )
    M21 =               CHI * np.real( np.fft.ifft2( CHI_hat * np.fft.fft2( H2 * H1a ) ) )
    #M21 = M12
    M22 = ( 1 - CHI ) + CHI * np.real( np.fft.ifft2( CHI_hat * np.fft.fft2( H2 * H2a ) ) )
    M23 =               CHI * np.real( np.fft.ifft2( CHI_hat * np.fft.fft2( H2 * H3a ) ) )
    M31 =               CHI * np.real( np.fft.ifft2( CHI_hat * np.fft.fft2( H3 * H1a ) ) )
    #M31 = M13
    M32 =               CHI * np.real( np.fft.ifft2( CHI_hat * np.fft.fft2( H3 * H2a ) ) )
    #M32 = M23
    M33 = ( 1 - CHI ) + CHI * np.real( np.fft.ifft2( CHI_hat * np.fft.fft2( H3 * H3a ) ) )

    #NOTE: There is a problem with the realization and numpy real and imaginary parts.
    b01 = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )
    b02 = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )
    b03 = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )
    bx1 = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )
    bx2 = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )
    bx3 = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )
    by1 = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )
    by2 = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )
    by3 = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )

    for i in range( 0, n_per_dim ):
        for j in range( 0, n_per_dim ):

            # Eq. 80 - Local momentum matrix
            M_local = build_matrix_M( M11[ i, j ], M12[ i , j ], M13[ i , j ], M21[ i, j ], M22[ i , j ], M23[ i , j ], M31[ i, j ], M32[ i , j ], M33[ i , j ] )
            #print( i, j )
            #print( f"i: {i}, x[i]: {X[i]}, CHI: {CHI[ i ]}, cond(M): {np.linalg.cond( M_local )}, M_local:" )
            print( f"i: {i}, cond(M): {np.linalg.cond( M_local )}, M_local:" )
            #print( M_local )
            #print( M_local )
            invM_local = np.linalg.inv( M_local )

            #TODO: fix this confusing indexing
            b01[ i, j ] = invM_local[ 0, 0 ]
            b02[ i, j ] = invM_local[ 0, 1 ]
            b03[ i, j ] = invM_local[ 0, 2 ]
            bx1[ i, j ] = ( -1 ) * invM_local[ 1, 0 ]
            bx2[ i, j ] = ( -1 ) * invM_local[ 1, 1 ]
            bx3[ i, j ] = ( -1 ) * invM_local[ 1, 2 ]
            by1[ i, j ] = ( -1 ) * invM_local[ 2, 0 ]
            by2[ i, j ] = ( -1 ) * invM_local[ 2, 1 ]
            by3[ i, j ] = ( -1 ) * invM_local[ 2, 2 ]

    return b01, b02, b03, bx1, bx2, bx3, by1, by2, by3

def eval_f_int( d, bx1, bx2, bx3, by1, by2, by3, CHI, V, H1a, H2a, H3a, H1a_bar, H2a_bar, H3a_bar ):

    n_per_dim = d.shape[ 0 ]

    d_hat = np.fft.fft2( CHI * d )

    Ax = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )
    Ay = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )

    # s dependent unrolled for
    D1 = np.real( np.fft.ifft2( d_hat * np.fft.fft2( H1a ) ) )
    D2 = np.real( np.fft.ifft2( d_hat * np.fft.fft2( H2a ) ) )
    D3 = np.real( np.fft.ifft2( d_hat * np.fft.fft2( H3a ) ) )
    Ax += bx1 * D1 + bx2 * D2 + bx3 * D3
    Ay += by1 * D1 + by2 * D2 + by3 * D3

    B_hat = np.fft.fft2( np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 ) )

    # s dependent unrolled for
    Cx1 = CHI * V * bx1
    Cx2 = CHI * V * bx2
    Cx3 = CHI * V * bx3
    Cy1 = CHI * V * by1
    Cy2 = CHI * V * by2
    Cy3 = CHI * V * by3
    B_hat += ( np.fft.fft2( Cx1 * Ax + Cy1 * Ay ) * np.fft.fft2( H1a_bar ) )
    B_hat += ( np.fft.fft2( Cx2 * Ax + Cy2 * Ay ) * np.fft.fft2( H2a_bar ) )
    B_hat += ( np.fft.fft2( Cx3 * Ax + Cy3 * Ay ) * np.fft.fft2( H3a_bar ) )

    f_int = CHI * np.real( np.fft.ifft2( B_hat ) )

    return f_int

def eval_f_r( r, b01, b02, b03, CHI, V, H1a_bar, H2a_bar, H3a_bar ):

    n_per_dim = r.shape[ 0 ]

    B_hat = np.fft.fft2( np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 ) )

    # s dependent unrolled for
    C01 = CHI * V * b01
    C02 = CHI * V * b02
    C03 = CHI * V * b03
    B_hat += ( np.fft.fft2( C01 * r ) *  np.fft.fft2( H1a_bar ) )
    B_hat += ( np.fft.fft2( C02 * r ) *  np.fft.fft2( H2a_bar ) )
    B_hat += ( np.fft.fft2( C03 * r ) *  np.fft.fft2( H3a_bar ) )

    f_r = CHI * np.real( np.fft.ifft2( B_hat ) )

    return f_r

def eval_field( d, b01, b02, b03, CHI, H1a, H2a, H3a ):

    n_per_dim = d.shape[ 0 ]

    d_hat = np.fft.fft2( CHI * d )
    u_h = np.zeros( [ n_per_dim, n_per_dim ], dtype=np.float64 )

    # s dependent unrolled for
    D1 = np.real( np.fft.ifft2( d_hat * np.fft.fft2( H1a ) ) )
    D2 = np.real( np.fft.ifft2( d_hat * np.fft.fft2( H2a ) ) )
    D3 = np.real( np.fft.ifft2( d_hat * np.fft.fft2( H3a ) ) )
    u_h += ( b01 * CHI ) * D1 + ( b02 * CHI ) * D2 + ( b03 * CHI ) * D3

    return u_h
