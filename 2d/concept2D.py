#! /usr/bin/env python3

import numpy as np

# subroutines2D
import subroutines2D
import print_and_debug2D
import plot2D

def rhs( x, y ):
    return ( 1. ) * ( 4. - 2. * x **2 - 2. * y**2 )
rhs_vectorized = np.vectorize( rhs )


if __name__ == "__main__":

    # Domain parameters
    n_per_dim = 64
    Lx = 2
    Ly = 2
    lex = 4 / ( n_per_dim - 2 )
    ley = 4 / ( n_per_dim - 2 )
    n_tot = n_per_dim * n_per_dim
    x_begin = -1
    y_begin = -1
    x_end_extended = x_begin + Lx + lex
    y_end_extended = y_begin + Ly + ley
    dx = ( Lx + lex ) / n_per_dim
    dy = ( Ly + ley ) / n_per_dim
    x_cords = np.array([ x_begin + i * dx for i in range( 0, n_per_dim ) ])
    y_cords = np.array([ y_begin + i * dy for i in range( 0, n_per_dim ) ])
    coef_a = 1.5 * dx
    dni_volume = dx * dy
    print( f"Lx: {Lx}, Ly: {Ly}, lex: {lex}, ley: {ley}, dx: {dx}, dy: {dy}, dni_volume: {dni_volume }" )
    print( f"lex = ( m_x + 1 ) * dx: { ((int)( coef_a ) + 1) * dx}\n" )
    print( f"x_cords: \n{x_cords}\ny_cords: \n{y_cords}\n" )

    # Characteristic functions + coordinates
    CHI = np.ones( [ n_per_dim, n_per_dim ], np.float64 )
    CHI_g = np.zeros( [ n_per_dim, n_per_dim ], np.float64 )
    X, Y = np.meshgrid( x_cords, y_cords )

    # set Dirichlet BC based on subsection 5.4.2 and Eq. 95 anad Eq. 96
    for x in range ( 0, n_per_dim ):
        for y in range ( 0, n_per_dim ):
            if ( X[ x, y ] < -1.0 ) or ( X[ x, y ] > 1.0 ) or ( X[ x, y ] < -1.0 ) or ( Y[ x, y ] > 1.0 ):
                CHI[ x, y ] = 0

            # boundary conditions map
            if ( X[ x, y ] > ( -1.0 - dx / 4 ) ) and ( X[ x, y ] < ( -1.0 + dx / 4 ) ) and ( Y[ x, y ] < 1 + dy / 4 ) or ( ( X[ x, y ] < ( 1.0 + dx / 4 ) ) and ( X[ x, y ] > ( 1.0 - dx / 4 ) ) ) and ( Y[ x, y ] < 1 + dy / 4 ):
                CHI_g[ x, y ] = 1

            if ( Y[ x, y ] > ( -1.0 - dy / 4 ) ) and ( Y[ x, y ] < ( -1.0 + dy / 4 ) ) and ( X[ x, y ] < 1 + dx / 4 ) or ( ( Y[ x, y ] < ( 1.0 + dy / 4 ) ) and ( Y[ x, y ] > ( 1.0 - dy / 4 ) ) ) and ( X[ x, y ] < 1 + dx / 4 ):
                CHI_g[ x, y ] = 1

    print_and_debug2D.plot_domain_setup( X, Y, CHI, CHI_g, False )

    # Definition of corner coordinates necessary for the adjustment, see Sec. 5.2, related do Fig. 7, Eq. 68
    cx = np.array( [ x_begin, x_end_extended, x_begin, x_end_extended ] )
    cy = np.array( [ y_begin, y_begin, y_end_extended, y_end_extended ] )

    ### ----- Test periodic adjustment of the domain
    print_and_debug2D.print_and_plot_periodic_adjustment( X, Y, cx, cy, coef_a, False )

    ### ----- Initialize node values, node volumes for DNI a RKPM arrays
    d = np.zeros( [ n_per_dim, n_per_dim ], np.float64 )
    V = np.ones( [ n_per_dim, n_per_dim ], np.float64 ) * dni_volume

    H1, H2, H3 = subroutines2D.create_H_vects( CHI, X, Y, cx, cy, coef_a )
    H1a, H2a, H3a = subroutines2D.create_Ha_vects( CHI, X, Y, cx, cy, coef_a )
    H1a_bar, H2a_bar, H3a_bar = subroutines2D.create_Ha_bar_vects( CHI, X, Y, cx, cy, coef_a )
    b01, b02, b03, bx1, bx2, bx3, by1, by2, by3 = subroutines2D.eval_b_vects( CHI, H1, H2, H3, H1a, H2a, H3a )
    print_and_debug2D.plot_precomputed_stuctures( H1, H2, H3, H1a, H2a, H3a, H1a_bar, H2a_bar, H3a_bar, False )

    ### ----- Conjugate gradient solver part
    res_s_history = []

    d = np.zeros( [ n_per_dim, n_per_dim ] )
    f_int = subroutines2D.eval_f_int( d, bx1, bx2, bx3, by1, by2, by3, CHI, V, H1a, H2a, H3a, H1a_bar, H2a_bar, H3a_bar ) * ( CHI - CHI_g )

    r = rhs_vectorized( X, Y )
    f_r = subroutines2D.eval_f_r( r, b01, b02, b03, CHI, V, H1a_bar, H2a_bar, H3a_bar ) * ( CHI - CHI_g )
    print_and_debug2D.plot_rhs_convoluted_and_init_force_rhs( r, f_r, f_int, False )

    # start the conjugate gradients iterations
    res = f_r.ravel() - f_int.ravel()
    p = res.copy();
    res_s_old = np.dot( res, res );
    print( f"Initial residuum: {res_s_old}" )

    for i in range( 0, 200 ):

        f_int_p = ( subroutines2D.eval_f_int( p.reshape( n_per_dim, n_per_dim ), bx1, bx2, bx3, by1, by2, by3, CHI, V, H1a, H2a, H3a, H1a_bar, H2a_bar, H3a_bar ) * ( CHI - CHI_g ) ).ravel()
        alpha = res_s_old / np.dot( p, f_int_p )
        d += alpha * p.reshape( n_per_dim, n_per_dim )
        res -= alpha * f_int_p

        res_s_new = np.dot( res, res )

        print( f'Step: {i}, new residue: {res_s_new}.' )
        if  res_s_new < 1e-06 :
            print( f'Converged in {i+1} iterations.' )
            break

        beta = res_s_new / res_s_old
        p = res + beta * p
        res_s_old = res_s_new

    u = subroutines2D.eval_field( d, b01, b02, b03, CHI, H1a, H2a, H3a ) * ( CHI - CHI_g )
    print( f"\nsolution:\n{u}")

    u_exact_func = lambda x, y: ( 1 - x**2 ) * ( 1 - y**2 )

    u_exact = u_exact_func( X, Y )

    plot2D.plot_results( X, Y, u, d, u_exact )
    #plot2D.plot_results( X[ 3:-2, 3:-2 ], Y[ 3:-2, 3:-2 ], u[ 3:-2, 3:-2 ], d[ 3:-2, 3:-2 ] )
