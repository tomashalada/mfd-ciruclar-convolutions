import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.gridspec as gridspec

import functions2D

def plot_domain_setup( X, Y, CHI, CHI_g, exit_after = True ):

    fig, axs = plt.subplots( 1, 4 )
    axs[ 0 ].imshow( CHI )
    axs[ 0 ].set_title( "CHI" )
    axs[ 1 ].imshow( CHI_g )
    axs[ 1 ].set_title( "CHI_g" )
    axs[ 2 ].imshow( X )
    axs[ 2 ].set_title( "X" )
    axs[ 3 ].imshow( Y )
    axs[ 3 ].set_title( "Y" )
    plt.show()

    if exit_after == True:
        exit()

def print_and_plot_periodic_adjustment( X, Y, cx, cy, coef_a, exit_after = True ):

    n_per_dim = X.shape[ 0 ]

    X_min = X[ 0, 0 ]
    X_max = X[ -1, -1 ]
    Y_min = Y[ 0, 0 ]
    Y_max = Y[ -1, -1 ]
    X_len = X_max - X_min
    Y_len = Y_max - Y_min
    X_c = X_min + X_len / 2
    Y_c = Y_min + Y_len / 2

    # Test corners and periodic domain adjustment:
    print( "chi_ci_vectorized( X, Y, 0 )" )
    print( functions2D.chi_ci_vectorized( X, Y, 0 ) )
    print( "chi_ci_vectorized( X, Y, 1 )" )
    print( functions2D.chi_ci_vectorized( X, Y, 1 ) )
    print( "chi_ci_vectorized( X, Y, 2 )" )
    print( functions2D.chi_ci_vectorized( X, Y, 2 ) )
    print( "chi_ci_vectorized( X, Y, 3 )" )
    print( functions2D.chi_ci_vectorized( X, Y, 3 ) )

    PHI_default = functions2D.func_PHI_vectorized( X, Y, coef_a )
    print( "PHI - default")
    print( PHI_default )
    plt.imshow( PHI_default )
    plt.show()

    PHI_adjusted = np.zeros( [ n_per_dim, n_per_dim ], np.float64 )
    PHI_adjusted = np.fft.fftshift( functions2D.func_PHI_vectorized( X - X_c, Y - Y_c, coef_a ) )

    print( "PHI - adjusted")
    print( PHI_adjusted )
    plt.imshow( PHI_adjusted )
    plt.show()

    if exit_after == True:
        exit()

def plot_precomputed_stuctures( H1, H2, H3, H1a, H2a, H3a, H1a_bar, H2a_bar, H3a_bar, exit_after = True ):

    fig, axs = plt.subplots( 3, 3 )
    axs[ 0, 0 ].imshow( H1 )
    axs[ 0, 0 ].set_title( "H1" )
    axs[ 0, 1 ].imshow( H2 )
    axs[ 0, 1 ].set_title( "H2" )
    axs[ 0, 2 ].imshow( H3 )
    axs[ 0, 2 ].set_title( "H3" )

    axs[ 1, 0 ].imshow( H1a )
    axs[ 1, 0 ].set_title( "H1a" )
    axs[ 1, 1 ].imshow( H2a )
    axs[ 1, 1 ].set_title( "H2a" )
    axs[ 1, 2 ].imshow( H3a )
    axs[ 1, 2 ].set_title( "H3a" )

    axs[ 2, 0 ].imshow( H1a_bar )
    axs[ 2, 0 ].set_title( "H1a_bar" )
    axs[ 2, 1 ].imshow( H2a_bar )
    axs[ 2, 1 ].set_title( "H2a_bar" )
    axs[ 2, 2 ].imshow( H3a_bar )
    axs[ 2, 2 ].set_title( "H3a_bar" )

    plt.show()

    if exit_after == True:
        exit()

def plot_rhs_convoluted_and_init_force_rhs( r, f_r, f_int, exit_after = False ):

    print( f"f_int:\n{f_int}\n" )
    print( f"rhs:\n{r}\nf_r:\n{f_r}\n" )

    fig, axs = plt.subplots( 1, 3 )
    fig.tight_layout( pad=2.0 )

    im = axs[ 0 ].imshow( r )
    divider = make_axes_locatable( axs[ 0 ] )
    cax = divider.append_axes( "right", size="5%", pad=0.05 )
    plt.colorbar( im, ax=axs[ 0 ], cax=cax )
    axs[ 0 ].set_title( "r" )

    im = axs[ 1 ].imshow( f_r )
    divider = make_axes_locatable( axs[ 1 ] )
    cax = divider.append_axes( "right", size="5%", pad=0.05 )
    plt.colorbar( im, ax=axs[ 1 ], cax=cax )
    axs[ 1 ].set_title( "f_ext" )

    im = axs[ 2 ].imshow( f_int )
    divider = make_axes_locatable( axs[ 2 ] )
    cax = divider.append_axes( "right", size="5%", pad=0.05 )
    plt.colorbar( im, ax=axs[ 2 ], cax=cax )
    axs[ 2 ].set_title( "f_int init" )

    plt.show()

    if exit_after == True:
        exit()
