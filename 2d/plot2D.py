import matplotlib.pyplot as plt

from matplotlib import cm
from matplotlib.ticker import LinearLocator

def plot_results( X, Y, u, d, u_exact ):

    # Plot parameters
    plt.rcParams.update({
      "text.usetex": True,
      "text.latex.preamble" : r"\usepackage{amsfonts}",
      "font.family": "Times",
      "font.serif" : "Times New Roman",
      "font.size"  : 14,
    })

    fig = plt.figure( figsize=( 15,6 ) )
    ax = fig.add_subplot(1, 2, 1, projection='3d')
    surf = ax.plot_surface( X, Y, u, rstride=1, cstride=1, cmap=cm.plasma, linewidth=0, antialiased=False, label=r"$u(x_i)$ num.", vmin=0, vmax=1 )
    ax.plot_wireframe( X[ 0 : -2 ], Y[ 0 : -2 ], u_exact[ 0 : -2 ], rstride=5, cstride=5, color="k", linewidth=0.2, label=r"$u(x)$ exact" )

    # Get rid of colored axes planes
    # First remove fill
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False

    # Now set color to white (or whatever is "invisible")
    ax.xaxis.pane.set_edgecolor('w')
    ax.yaxis.pane.set_edgecolor('w')
    ax.zaxis.pane.set_edgecolor('w')

    plt.colorbar( surf, shrink=0.6, aspect=10 )
    plt.xlabel( "X" )
    plt.ylabel( "Y" )
    plt.clabel( r"u" )
    #plt.legend()
    ax.set_proj_type('persp', focal_length=0.5)
    plt.title( "Interpolated solution", fontsize=18 )

    ax = fig.add_subplot(1, 2, 2, projection='3d')
    #surf_d = ax.plot_surface( X, Y, d, rstride=1, cstride=1, cmap=cm.seismic, linewidth=0, antialiased=False )
    ax.scatter( X.ravel(), Y.ravel(), d, color='b', s=0.75, label=r"$d_i$ - nodal vals."  )

    # Get rid of colored axes planes
    # First remove fill
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False

    # Now set color to white (or whatever is "invisible")
    ax.xaxis.pane.set_edgecolor('w')
    ax.yaxis.pane.set_edgecolor('w')
    ax.zaxis.pane.set_edgecolor('w')

    plt.xlabel( "X" )
    plt.ylabel( "Y" )
    plt.clabel( r"d" )
    ax.set_proj_type('persp', focal_length=0.5)
    plt.title( "Nodal values", fontsize=18 )

    ## add a color bar which maps values to colors.
    #fig.colorbar( surf, shrink=0.5, aspect=10 )

    plt.savefig("results_2d_fcrkpm.png", bbox_inches='tight')
