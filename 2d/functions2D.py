import numpy as np

def build_matrix_M( m11, m12, m13, m21, m22, m23, m31, m32, m33 ):
    return np.array( [ [ m11, m12, m13 ],
                       [ m21, m22, m23 ],
                       [ m31, m32, m33 ] ] )

# Eq. 7 - Kernel function.
def phi( x, h ):
    #h = h * 1.4
    z = abs( x ) / h
    if 0 <= z < 0.5:
        return 2.0 / 3.0  - 4.0 * z * z + 4.0 * z * z * z
    elif 0.5 <= z < 1:
        return 4.0 / 3.0 - 4 * z + 4 * z * z - 4.0 / 3.0 * z * z
    else:
        return 0
phi_vectorized = np.vectorize( phi, otypes=[ np.float64 ] )

# Eq. 69 - Corner partitions characteristic function
def chi_ci( x, y, corner ):
    if corner == 0:
        if x <= 0 and y <= 0:
            return 1
        else:
            return 0
    if corner == 1:
        if x > 0 and y <= 0:
            return 1
        else:
            return 0
    if corner == 2:
        if x <= 0 and y > 0:
            return 1
        else:
            return 0
    if corner == 3:
        if x > 0 and y > 0:
            return 1
        else:
            return 0
chi_ci_vectorized = np.vectorize( chi_ci, excluded=[ "corner" ],  otypes=[ np.float64 ]  )

#: # Monomial H1 with corner shift (introduced within the eq. 77 )
#: def func_H1( x, y, ci_x, ci_y ):
#:     return 1
#: func_H1_vectorized = np.vectorize( func_H1, excluded=[ "ci_x", "ci_y" ] )
#:
#: # Monomial H2 with corner shift (introduced within the eq. 77 )
#: def func_H2( x, y, ci_x, ci_y ):
#:     return x - ci_x
#: func_H2_vectorized = np.vectorize( func_H2, excluded=[ "ci_x", "ci_y" ] )
#:
#: # Monomial H3 with corner shift (introduced within the eq. 77 )
#: def func_H3( x, y, ci_x, ci_y ):
#:     return y - ci_y
#: func_H3_vectorized = np.vectorize( func_H3, excluded=[ "ci_x", "ci_y" ] )
#:
#: # Kernel functions with corner shift (introduced within the eq. 77 )
#: def func_PHI( x, y, ci_x, ci_y, h ):
#:     return phi_vectorized( x - ci_x, h ) * phi_vectorized( y - ci_y, h )
#: func_PHI_vectorized = np.vectorize( func_PHI, excluded=[ "ci_x", "ci_y", "h" ] )
#:
#: # Monomial H1 with corner shift (introduced within the eq. 77 )
#: def func_H1_bar( x, y, ci_x, ci_y ):
#:     return 1
#: func_H1_bar_vectorized = np.vectorize( func_H1_bar, excluded=[ "ci_x", "ci_y" ] )
#:
#: # Monomial H2 with corner shift (introduced within the eq. 77 )
#: def func_H2_bar( x, y, ci_x, ci_y ):
#:     return ( -1 ) * ( x - ci_x )
#: func_H2_bar_vectorized = np.vectorize( func_H2_bar, excluded=[ "ci_x", "ci_y" ] )
#:
#: # Monomial H3 with corner shift (introduced within the eq. 77 )
#: def func_H3_bar( x, y, ci_x, ci_y ):
#:     return ( -1 ) * ( y - ci_y )
#: func_H3_bar_vectorized = np.vectorize( func_H3_bar, excluded=[ "ci_x", "ci_y" ] )
#:
#: # Kernel functions with corner shift (introduced within the eq. 77 )
#: def func_PHI_bar( x, y, ci_x, ci_y, h ):
#:     return phi_vectorized( ( -1 ) * ( x - ci_x ), h ) * phi_vectorized( ( -1 ) * ( y - ci_y ), h )
#: func_PHI_bar_vectorized = np.vectorize( func_PHI_bar, excluded=[ "ci_x", "ci_y", "h" ] )


# Monomial H1 with corner shift (introduced within the eq. 77 )
def func_H1( x, y ):
    return 1
func_H1_vectorized = np.vectorize( func_H1, otypes=[ np.float64 ] )

# Monomial H2 with corner shift (introduced within the eq. 77 )
def func_H2( x, y ):
    return x
func_H2_vectorized = np.vectorize( func_H2, otypes=[ np.float64 ] )

# Monomial H3 with corner shift (introduced within the eq. 77 )
def func_H3( x, y ):
    return y
func_H3_vectorized = np.vectorize( func_H3, otypes=[ np.float64 ] )

# Kernel functions with corner shift (introduced within the eq. 77 )
def func_PHI( x, y, h ):
    return phi_vectorized( x, h ) * phi_vectorized( y, h )
func_PHI_vectorized = np.vectorize( func_PHI, excluded=[ "h" ], otypes=[ np.float64 ] )
