import numpy as np
import matplotlib.pyplot as plt

import functions1D

def print_and_plot_periodic_adjustment( CHI, X, cx,  coef_a, exit_after = True ):

    n_per_dim = X.shape[ 0 ]
    x_min = X[ 0 ]
    x_max = X[ -1 ]
    x_len = x_max - x_min
    x_c = x_min + x_len / 2

    # Test corners and periodic domain adjustment:
    print( "chi_ci_vectorized( X, 0 ):" )
    print( functions1D.chi_ci_vectorized( X, x_c, 0 ) )
    print( "chi_ci_vectorized( X, 1 ):" )
    print( functions1D.chi_ci_vectorized( X, x_c, 1 ) )

    fig, axs = plt.subplots( 1, 3 )
    axs[ 0 ].scatter( X, CHI )
    axs[ 0 ].set_title( "CHI" )

    PHI_default = functions1D.func_PHI_vectorized( X - x_c, coef_a )
    print( "PHI - default:")
    print( PHI_default )
    axs[ 1 ].scatter( X, PHI_default )
    axs[ 1 ].set_title( "PHI" )

    PHI_adjusted = np.zeros( [ n_per_dim ], np.float64 )

    #PHI_adjusted += functions1D.chi_ci_vectorized( X, 0 ) * functions1D.func_PHI_vectorized( X - X, coef_a )
    #PHI_adjusted += functions1D.chi_ci_vectorized( X, 1 ) * functions1D.func_PHI_vectorized( X - X, coef_a )

    #PHI_adjusted += functions1D.chi_ci_vectorized( X, 0 ) * functions1D.func_PHI_vectorized( X - cx[ 0 ], coef_a )
    #PHI_adjusted += functions1D.chi_ci_vectorized( X, 1 ) * functions1D.func_PHI_vectorized( X - cx[ 1 ], coef_a )
    PHI_adjusted = np.fft.fftshift( functions1D.func_PHI_vectorized( X - x_c, coef_a ) )

    #PHI_adjusted += functions1D.chi_ci_vectorized( X, 0 ) * functions1D.func_PHI_vectorized( X - ( X - x_len / 2 ), coef_a )
    #PHI_adjusted += functions1D.chi_ci_vectorized( X, 1 ) * functions1D.func_PHI_vectorized( X - ( X + x_len / 2 ), coef_a )

    #PHI_adjusted += functions1D.chi_ci_vectorized( X, 0 ) * functions1D.func_PHI_vectorized( X - x_c, coef_a )
    #PHI_adjusted += functions1D.chi_ci_vectorized( X, 1 ) * functions1D.func_PHI_vectorized( X - x_c, coef_a )

    print( "PHI - adjusted:")
    print( PHI_adjusted )
    axs[ 2 ].scatter( X, PHI_adjusted )
    axs[ 2 ].set_title( "PHI adjusted" )
    plt.show()

    if exit_after == True:
        exit()

def plot_precomputed_stuctures_H_vectors( X, H1, H2, H3, H1a, H2a, H3a, H1a_bar, H2a_bar, H3a_bar, exit_after = True ):

    print( f"H1:\n{H1}\nH2:{H2}\n" )
    print( f"H1a:\n{H1a}\nH2a:{H2a}\n" )
    print( f"H1a_bar:\n{H1a_bar}\nH2a_bar:{H2a_bar}\n" )

    fig, axs = plt.subplots( 3, 3 )
    fig.tight_layout( pad=1.3 )

    axs[ 0, 0 ].scatter( X, H1 )
    axs[ 0, 0 ].set_title( "H1" )
    axs[ 0, 1 ].scatter( X, H2 )
    axs[ 0, 1 ].set_title( "H2" )
    axs[ 0, 2 ].scatter( X, H3 )
    axs[ 0, 2 ].set_title( "H3" )

    axs[ 1, 0 ].scatter( X, H1a )
    axs[ 1, 0 ].set_title( "H1a" )
    axs[ 1, 1 ].scatter( X, H2a )
    axs[ 1, 1 ].set_title( "H2a" )
    axs[ 1, 2 ].scatter( X, H3a )
    axs[ 1, 2 ].set_title( "H3a" )

    axs[ 2, 0 ].scatter( X, H1a_bar )
    axs[ 2, 0 ].set_title( "H1a_bar" )
    axs[ 2, 1 ].scatter( X, H2a_bar )
    axs[ 2, 1 ].set_title( "H2a_bar" )
    axs[ 2, 2 ].scatter( X, H3a_bar )
    axs[ 2, 2 ].set_title( "H3a_bar" )

    plt.show()

    if exit_after == True:
        exit()

def plot_precomputed_stuctures_b_vectors( X, b01, b02, b03, bx1, bx2, bx3, exit_after = True ):

    print( f"b01:\n{b01}\nb02:\n{b02}\nbx1:\n{bx1}\nbx2:\n{bx2}\n" )

    fig, axs = plt.subplots( 2, 3 )
    fig.tight_layout( pad=1.3 )

    axs[ 0, 0 ].scatter( X, b01 )
    axs[ 0, 0 ].set_title( "b01" )
    axs[ 0, 1 ].scatter( X, b02 )
    axs[ 0, 1 ].set_title( "b02" )
    axs[ 0, 2 ].scatter( X, b03 )
    axs[ 0, 2 ].set_title( "b03" )

    axs[ 1, 0 ].scatter( X, bx1 )
    axs[ 1, 0 ].set_title( "bx1" )
    axs[ 1, 1 ].scatter( X, bx2 )
    axs[ 1, 1 ].set_title( "b2x" )
    axs[ 1, 2 ].scatter( X, bx3 )
    axs[ 1, 2 ].set_title( "b3x" )

    plt.show()

    if exit_after == True:
        exit()

def plot_precomputed_stuctures_M_matrices( X, M11, M12, M13, M21, M22, M23, M31, M32, M33, exit_after = True ):

    print( f"M11:\n{M11}\nM12:\n{M12}\nM21:\n{M21}\nM22:\n{M22}\n" )

    fig, axs = plt.subplots( 3, 3 )
    fig.tight_layout( pad=1.3 )

    axs[ 0, 0 ].scatter( X, M11 )
    axs[ 0, 0 ].set_title( "M11" )
    axs[ 0, 1 ].scatter( X, M12 )
    axs[ 0, 1 ].set_title( "M12" )
    axs[ 0, 2 ].scatter( X, M13 )
    axs[ 0, 2 ].set_title( "M13" )

    axs[ 1, 0 ].scatter( X, M21 )
    axs[ 1, 0 ].set_title( "M21" )
    axs[ 1, 1 ].scatter( X, M22 )
    axs[ 1, 1 ].set_title( "M22" )
    axs[ 1, 2 ].scatter( X, M23 )
    axs[ 1, 2 ].set_title( "M23" )

    axs[ 2, 0 ].scatter( X, M31 )
    axs[ 2, 0 ].set_title( "M31" )
    axs[ 2, 1 ].scatter( X, M32 )
    axs[ 2, 1 ].set_title( "M32" )
    axs[ 2, 2 ].scatter( X, M33 )
    axs[ 2, 2 ].set_title( "M33" )

    plt.show()

    if exit_after == True:
        exit()

def plot_rhs_convoluted_and_init_force_rhs( X, r, f_r, f_int, exit_after = False ):

    print( f"f_int:\n{f_int}\n" )
    print( f"rhs:\n{r}\nf_r:\n{f_r}\n" )

    fig, axs = plt.subplots( 1, 3 )
    fig.tight_layout( pad=2.0 )

    axs[ 0 ].scatter( X, r )
    axs[ 0 ].set_title( "rhs" )
    #divider = make_axes_locatable( axs[ 0 ] )
    #cax = divider.append_axes( "right", size="5%", pad=0.05 )
    #plt.colorbar( im, ax=axs[ 0 ], cax=cax )

    axs[ 1 ].scatter( X, f_r )
    axs[ 1 ].set_title( "f_r" )
    #divider = make_axes_locatable( axs[ 1 ] )
    #cax = divider.append_axes( "right", size="5%", pad=0.05 )
    #plt.colorbar( im, ax=axs[ 1 ], cax=cax )

    axs[ 2 ].scatter( X, f_int )
    axs[ 2 ].set_title( "f_int" )
    #divider = make_axes_locatable( axs[ 2 ] )
    #cax = divider.append_axes( "right", size="5%", pad=0.05 )
    #plt.colorbar( im, ax=axs[ 2 ], cax=cax )

    plt.show()

    if exit_after == True:
        exit()
