import numpy as np
from functions1D import *
import print_and_debug1D

def create_H_vects( CHI, X, cx, coef_a ):

    n_per_dim = CHI.shape[ 0 ]
    x_min = X[ 0 ]
    x_max = X[ -1 ]
    x_len = x_max - x_min
    x_c = x_min + x_len / 2

    H1 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    H2 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    H3 = np.zeros( [ n_per_dim ], dtype=np.float64 )

    # Eq. 79 - adjusted monomial basis functions
    for corner in range( 0 , 2 ):
        #H1 +=  chi_ci_vectorized( X, x_c, corner ) * func_H1_vectorized( X - cx[ corner ] )
        #H2 +=  chi_ci_vectorized( X, x_c, corner ) * func_H2_vectorized( X - cx[ corner ] )

        H1 +=  chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( X - x_c ) )
        H2 +=  chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( X - x_c ) )
        H3 +=  chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H3_vectorized( X - x_c ) )

        #H1 += np.fft.fftshift( func_H1_vectorized( X - x_c ) )
        #H2 += np.fft.fftshift( func_H2_vectorized( X - x_c ) )

    return H1, H2, H3

def create_Ha_vects( CHI, X, cx, coef_a ):

    n_per_dim = CHI.shape[ 0 ]
    x_min = X[ 0 ]
    x_max = X[ -1 ]
    x_len = x_max - x_min
    x_c = x_min + x_len / 2

    H1a = np.zeros( [ n_per_dim ], dtype=np.float64 )
    H2a = np.zeros( [ n_per_dim ], dtype=np.float64 )
    H3a = np.zeros( [ n_per_dim ], dtype=np.float64 )

    # Eq. 78 - adjusted monomial basis functions
    for corner in range( 0 , 2 ):
        #H1a +=  chi_ci_vectorized( X, x_c, corner ) * func_H1_vectorized( X - cx[ corner ] ) * func_PHI_vectorized( X - cx[ corner ], coef_a )
        #H2a +=  chi_ci_vectorized( X, x_c, corner ) * func_H2_vectorized( X - cx[ corner ] ) * func_PHI_vectorized( X - cx[ corner ], coef_a )

        #H1a += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( X - x_c ) ) * np.fft.fftshift( func_PHI_vectorized( X - x_c, coef_a ) )
        #H2a += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( X - x_c ) ) * np.fft.fftshift( func_PHI_vectorized( X - x_c, coef_a ) )

        #H1a +=  chi_ci_vectorized( X, x_c, corner ) * func_H1_vectorized( X - cx[ corner ] ) * np.fft.fftshift( func_PHI_vectorized( X - x_c, coef_a ) )
        #H2a +=  chi_ci_vectorized( X, x_c, corner ) * func_H2_vectorized( X - cx[ corner ] ) * np.fft.fftshift( func_PHI_vectorized( X - x_c, coef_a ) )

        H1a += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( X - x_c ) * func_PHI_vectorized( X - x_c, coef_a ) )
        H2a += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( X - x_c ) * func_PHI_vectorized( X - x_c, coef_a ) )
        H3a += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H3_vectorized( X - x_c ) * func_PHI_vectorized( X - x_c, coef_a ) )
        #H1a +=  np.fft.fftshift( func_H1_vectorized( X - x_c ) * func_PHI_vectorized( X - x_c, coef_a ) )
        #H2a +=  np.fft.fftshift( func_H2_vectorized( X - x_c ) * func_PHI_vectorized( X - x_c, coef_a ) )

    return H1a, H2a, H3a

def create_Ha_bar_vects( CHI, X, cx, coef_a ):

    n_per_dim = CHI.shape[ 0 ]
    x_min = X[ 0 ]
    x_max = X[ -1 ]
    x_len = x_max - x_min
    x_c = x_min + x_len / 2

    H1a_bar = np.zeros( [ n_per_dim ], dtype=np.float64 )
    H2a_bar = np.zeros( [ n_per_dim ], dtype=np.float64 )
    H3a_bar = np.zeros( [ n_per_dim ], dtype=np.float64 )

    # Eq. 79 - adjusted bar monomial basis functions
    for corner in range( 0 , 2 ):
        #H1a_bar += chi_ci_vectorized( X, x_c, corner ) * func_H1_vectorized( ( -1 ) * ( X - cx[ corner ] ) ) * func_PHI_vectorized( ( -1 ) * ( X - cx[ corner ] ), coef_a )
        #H2a_bar += chi_ci_vectorized( X, x_c, corner ) * func_H2_vectorized( ( -1 ) * ( X - cx[ corner ] ) ) * func_PHI_vectorized( ( -1 ) * ( X - cx[ corner ] ), coef_a )

        #H1a_bar += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( ( -1 ) * ( X - x_c ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
        #H2a_bar += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( ( -1 ) * ( X - x_c ) ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )

        #H1a_bar += chi_ci_vectorized( X, x_c, corner ) * func_H1_vectorized( ( -1 ) * ( X - cx[ corner ] ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
        #H2a_bar += chi_ci_vectorized( X, x_c, corner ) * func_H2_vectorized( ( -1 ) * ( X - cx[ corner ] ) ) * np.fft.fftshift( func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )

        H1a_bar += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H1_vectorized( ( -1 ) * ( X - x_c ) ) * func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
        H2a_bar += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H2_vectorized( ( -1 ) * ( X - x_c ) ) * func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
        H3a_bar += chi_ci_vectorized( X, x_c, corner ) * np.fft.fftshift( func_H3_vectorized( ( -1 ) * ( X - x_c ) ) * func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
        #H1a_bar += np.fft.fftshift( func_H1_vectorized( ( -1 ) * ( X - x_c ) ) * func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )
        #H2a_bar += np.fft.fftshift( func_H2_vectorized( ( -1 ) * ( X - x_c ) ) * func_PHI_vectorized( ( -1 ) * ( X - x_c ), coef_a ) )

    return H1a_bar, H2a_bar, H3a_bar

def eval_b_vects( CHI, H1, H2, H3, H1a, H2a, H3a, X ):

    n_per_dim = CHI.shape[ 0 ]

    CHI_hat = np.fft.fft( CHI )

    # unrolled for p in [ 1, s ]
    # unrolled for q in [ p, s ]
    M11 = ( 1 - CHI ) + CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H1 * H1a ) ) )
    M12 =               CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H1 * H2a ) ) )
    M13 =               CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H1 * H3a ) ) )
    M21 =               CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H2 * H1a ) ) )
    M22 = ( 1 - CHI ) + CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H2 * H2a ) ) )
    M23 =               CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H2 * H3a ) ) )
    M31 =               CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H3 * H1a ) ) )
    M32 =               CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H3 * H2a ) ) )
    M33 = ( 1 - CHI ) + CHI * np.real( np.fft.ifft( CHI_hat * np.fft.fft( H3 * H3a ) ) )
    #print_and_debug1D.plot_precomputed_stuctures_M_matrices( X, M11, M12, M21, M22, False )

    #NOTE: There is a problem with the realization and numpy real and imaginary parts.
    b01 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    b02 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    b03 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    bx1 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    bx2 = np.zeros( [ n_per_dim ], dtype=np.float64 )
    bx3 = np.zeros( [ n_per_dim ], dtype=np.float64 )

    for i in range( 0, n_per_dim ):

        # Eq. 80 - Local momentum matrix
        M_local = build_matrix_M( M11[ i ], M12[ i ], M13[ i ], M21[ i ], M22[ i ], M23[ i ], M31[ i ], M32[ i ], M33[ i ] )
        #print( np.linalg.cond( M_local ) )
        print( f"i: {i}, CHI: {CHI[ i ]}, cond(M): {np.linalg.cond( M_local )}, M_local:" )
        print( M_local )
        if check_symmetric( M_local ) == False:
            exit( "M_local is not symmetric!" )
        invM_local = np.linalg.inv( M_local )

        #TODO: fix this confusing indexing
        b01[ i ] = invM_local[ 0, 0 ]
        b02[ i ] = invM_local[ 0, 1 ]
        b03[ i ] = invM_local[ 0, 2 ]
        bx1[ i ] = - invM_local[ 1, 0 ]
        bx2[ i ] = - invM_local[ 1, 1 ]
        bx3[ i ] = - invM_local[ 1, 2 ]

    return b01, b02, b03, bx1, bx2, bx3

def eval_f_int( d, bx1, bx2, bx3, CHI, V, H1a, H2a, H3a, H1a_bar, H2a_bar, H3a_bar ):

    n_per_dim = d.shape[ 0 ]

    d_hat = np.fft.fft( CHI * d )

    Ax = np.zeros( [ n_per_dim ], dtype=np.float64 )

    # s dependent unrolled for
    D1 = np.real( np.fft.ifft( d_hat * np.fft.fft( H1a ) ) )
    D2 = np.real( np.fft.ifft( d_hat * np.fft.fft( H2a ) ) )
    D3 = np.real( np.fft.ifft( d_hat * np.fft.fft( H3a ) ) )
    Ax += bx1 * D1 + bx2 * D2 + bx3 * D3

    B_hat = np.fft.fft( np.zeros( [ n_per_dim ], dtype=np.float64 ) )

    # s dependent unrolled for
    Cx1 = CHI * V * bx1
    Cx2 = CHI * V * bx2
    Cx3 = CHI * V * bx3
    B_hat += ( np.fft.fft( Cx1 * Ax ) * np.fft.fft( H1a_bar ) )
    B_hat += ( np.fft.fft( Cx2 * Ax ) * np.fft.fft( H2a_bar ) )
    B_hat += ( np.fft.fft( Cx3 * Ax ) * np.fft.fft( H3a_bar ) )

    f_int = CHI * np.real( np.fft.ifft( B_hat ) )

    return f_int

def eval_f_r( r, b01, b02, b03, CHI, V, H1a_bar, H2a_bar, H3a_bar ):

    n_per_dim = r.shape[ 0 ]

    B_hat = np.fft.fft( np.zeros( [ n_per_dim ], dtype=np.float64 ) )

    # s dependent unrolled for
    C01 = CHI * V * b01
    C02 = CHI * V * b02
    C03 = CHI * V * b03
    B_hat += ( np.fft.fft( C01 * r ) * np.fft.fft( H1a_bar ) )
    B_hat += ( np.fft.fft( C02 * r ) * np.fft.fft( H2a_bar ) )
    B_hat += ( np.fft.fft( C03 * r ) * np.fft.fft( H3a_bar ) )

    f_r = CHI * np.real( np.fft.ifft( B_hat ) )

    return f_r

def eval_field( d, b01, b02, b03, CHI, H1a, H2a, H3a ):

    n_per_dim = d.shape[ 0 ]

    d_hat = np.fft.fft( CHI * d )
    u_h = np.zeros( [ n_per_dim ], dtype=np.float64 )

    # s dependent unrolled for
    D1 = np.real( np.fft.ifft( d_hat * np.fft.fft( H1a ) ) )
    D2 = np.real( np.fft.ifft( d_hat * np.fft.fft( H2a ) ) )
    D3 = np.real( np.fft.ifft( d_hat * np.fft.fft( H3a ) ) )
    u_h += ( b01 * CHI ) * D1 + ( b02 * CHI ) * D2 + ( b03 * CHI ) * D3

    return u_h
