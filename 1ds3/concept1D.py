#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

# subroutines
import subroutines1D
import print_and_debug1D
import plot1D

def rhs( x ):
    return ( 2. )
rhs_vectorized = np.vectorize( rhs )

if __name__ == "__main__":

    # domain parameters
    n_per_dim = 128
    Lx = 2
    #lex = 4 / ( n_per_dim - 2 )
    lex = 4 / ( n_per_dim - 2 )
    n_tot = n_per_dim
    x_begin = -1
    x_end_extended = x_begin + Lx + lex
    dx = ( Lx + lex ) / n_per_dim
    x_cords = np.linspace( x_begin, x_end_extended, n_per_dim )
    coef_a_const = 1.5
    coef_a = coef_a_const * dx
    dni_volume = dx
    print( f"Lx: {Lx}, lex: {lex}, dx: {dx}, dni_volume: {dni_volume }" )
    print( f"lex = ( m_x + 1 ) * dx: { (np.floor( coef_a_const ) + 1) * dx}\n" )
    print( f"x_cords: \n{x_cords}\n" )
    CHI = np.ones( [ n_per_dim ] )

    # setup boundary map
    CHI_g = np.zeros( [ n_per_dim ] )
    CHI_g[ 0 ] = 1; CHI_g[ -3 ] = 1
    CHI -= CHI_g
    print( CHI )

    X = np.copy( x_cords )

    # Definition of corner coordinates necessary for the adjustment, see Sec. 5.2, related do Fig. 7, Eq. 68
    cx = np.array( [ x_begin, x_end_extended ] )

    ### ----- Test periodic adjustment of the domain
    print_and_debug1D.print_and_plot_periodic_adjustment( CHI, X, cx, coef_a, False )

    ### ----- Initialize node values, node volumes for DNI a RKPM arrays
    d = np.zeros( [ n_per_dim ], np.float64 )
    V = np.ones( [ n_per_dim ], np.float64 ) * dni_volume

    H1, H2, H3 = subroutines1D.create_H_vects( CHI, X, cx, coef_a )
    H1a, H2a, H3a = subroutines1D.create_Ha_vects( CHI, X, cx, coef_a )
    H1a_bar, H2a_bar, H3a_bar = subroutines1D.create_Ha_bar_vects( CHI, X, cx, coef_a )
    print_and_debug1D.plot_precomputed_stuctures_H_vectors( X, H1, H2, H3, H1a, H2a, H3a, H1a_bar, H2a_bar, H3a_bar, False )

    b01, b02, b03, bx1, bx2, bx3 = subroutines1D.eval_b_vects( CHI, H1, H2, H3, H1a, H2a, H3a, X )
    print_and_debug1D.plot_precomputed_stuctures_b_vectors( X, b01, b02, b03, bx1, bx2, bx3, False )
    #CHI -= CHI_g

    # ------- conjugate gradient solver part -------
    res_s_history = []

    f_int = subroutines1D.eval_f_int( d, bx1, bx2, bx3, CHI, V, H1a, H2a, H3a, H1a_bar, H2a_bar, H3a_bar )
    r = rhs_vectorized( X )
    f_r = subroutines1D.eval_f_r( r, b01, b02, b03, CHI, V, H1a_bar, H2a_bar, H3a_bar )
    print_and_debug1D.plot_rhs_convoluted_and_init_force_rhs( X, r, f_r, f_int, False )
    res = f_r - f_int
    p = res.copy();
    res_s_old = np.dot( res, res );
    print( f"Initial p:\n{p}\n" )
    print( f"Initial residuum: {res_s_old}" )

    #CHI -= CHI_g
    for i in range( 0, 1 ):

        f_int_p = subroutines1D.eval_f_int( p, bx1, bx2, bx3, CHI, V, H1a, H2a, H3a, H1a_bar, H2a_bar, H3a_bar )
        print( f"> iteration i: {i}, f_int_p:\n{f_int_p}" )
        alpha = res_s_old / np.dot( p, f_int_p )
        d += alpha * p
        res -= alpha * f_int_p

        res_s_new = np.dot( res, res )

        print( res_s_new )
        if  np.sqrt( res_s_new < 1e-6 ):
            print( f'Converged in {i+1} iterations.' )
            break

        beta = res_s_new / res_s_old
        p = res + beta * p
        res_s_old = res_s_new

    u = subroutines1D.eval_field( d, b01, b02, b03, CHI, H1a, H2a, H3a )
    print( f"\nsolution:\n{u}")

    plot1D.plot_results( X, u, d )
