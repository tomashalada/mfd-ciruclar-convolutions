import matplotlib.pyplot as plt

from matplotlib import cm
from matplotlib.ticker import LinearLocator

def plot_results( X, u, d ):

    fig = plt.figure()
    #ax = fig.add_subplot( 211, projection='3d' )
    ax = fig.add_subplot(1, 2, 1 )
    #surf = ax.plot_surface( X, Y, u, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False )
    #NOTE: Alternative plot using scatter only
    sc = ax.scatter( X, u, c=u, cmap=cm.coolwarm, linewidth=0, antialiased=False )
    plt.colorbar( sc, shrink=0.2, aspect=10 )
    plt.xlabel( "X" )
    plt.ylabel( "u" )
    plt.title( "u_h" )

    ax = fig.add_subplot(1, 2, 2 )
    #surf_d = ax.plot_surface( X, Y, d, rstride=1, cstride=1, cmap=cm.seismic, linewidth=0, antialiased=False )
    ax.scatter( X.ravel(), d )
    plt.xlabel( "X" )
    plt.ylabel( "d" )
    plt.title( "coef d" )

    ## add a color bar which maps values to colors.
    #fig.colorbar( surf, shrink=0.5, aspect=10 )

    plt.show()
    #plt.savefig( "res.png" )
