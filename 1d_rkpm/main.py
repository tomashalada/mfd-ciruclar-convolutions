#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt


# Eq. 7 - Kernel function.
def phi( x, h ):
    z = abs( x ) / h
    if 0 <= z < 0.5:
        return 2.0 / 3.0  - 4.0 * z * z + 4.0 * z * z * z
    elif 0.5 <= z < 1:
        return 4.0 / 3.0 - 4 * z + 4 * z * z - 4.0 / 3.0 * z * z * z
    else:
        return 0

def dPhi( x, h ):
    z = abs( x ) / h
    dz = np.sign(x) * 1 / h;
    if 0 <= z < 0.5:
        return -8.0 * z * dz + 12.0 * z * z * 2.0 * dz;
    elif 0.5 <= z < 1:
        return -4.0 * dz + 8.0 * z * dz - 4.0 * z * z * 2.0 * dz;
    else:
        return 0

basis = 'linear'
implicit_gradient = True

## Linear basis
if basis == 'linear':
    # basis size
    s = 2

    # mapping vectors
    C0 = np.array( [ 1, 0 ] ).T
    Cdx = np.array( [ 0, -1 ] ).T

    # eval linear basis
    def H( x ):
        return np.array( [[ 1 , x ]] ).T

    def dH( x ):
        return np.array( [[ 0 , 1 ]] ).T

## Quadratic basis
#if basis == 'quadratic':
#    # basis size
#    s = 3
#
#    # mapping vectors
#    C0 = np.array( [ 1, 0, 0 ] ).T
#    Cdx = np.array( [ 0, -1, 0 ] ).T
#
#    # eval quadratic basis
#    def H( x ):
#        return np.array( [[ 1 , x , x**2 ]] ).T

if __name__ == "__main__":

    # Alternative setup to keep the dx same as for the extended domain
    n_per_dim_extended = 64
    n_per_dim = n_per_dim_extended - 2
    Lx = 2
    lex = 4 / ( n_per_dim_extended - 2 )
    x_begin = -1
    x_end = x_begin + Lx + lex
    x_end_extended = x_begin + Lx + lex
    dx = ( Lx + lex ) / n_per_dim_extended
    X = np.array( [ x_begin + i * dx for i in range( 0, n_per_dim + 1 ) ] )

    # kernel support size coef
    coef_a_const = 1.5
    # kernel support size
    coef_a = coef_a_const * dx
    dni_volume = dx

    print( f"Lx: {Lx},dx: {dx}, coef_a: {coef_a}, dni_volume: {dni_volume }, x_end: {x_end}" )
    print( f"x_cords: \n{X}\n" )

    # Right hand side
    r = 2 * np.ones( [ n_per_dim + 1 ] )

    # List of matrices [N points, s, s ]
    Minv_list = np.zeros( [ n_per_dim + 1, s, s  ] )
    dMinv_list = np.zeros( [ n_per_dim + 1, s, s  ] )

    for i in range( 0, n_per_dim + 1 ):

        # Since we assume that particles are quidistant and the kernle size
        # takes into account only neihgboring paritcles, we can directly access the neighbors (left x_m and right x_p)
        if i == 0:

            x_z = X[ i ]
            x_p = X[ i + 1 ]

            H_z = H( x_z - x_z )
            phi_z = phi( x_z - x_z, coef_a )
            H_p = H( x_z - x_p )
            phi_p = phi( x_z - x_p,  coef_a )

            M_loc = H_z @ H_z.T * phi_z + H_p @ H_p.T * phi_p

        elif i == n_per_dim :

            x_m = X[ i - 1 ]
            x_z = X[ i ]

            H_m = H( x_z - x_m )
            phi_m = phi( x_z - x_m, coef_a )
            H_z = H( x_z - x_z )
            phi_z = phi( x_z - x_z, coef_a )

            M_loc = H_m @ H_m.T * phi_m + H_z @ H_z.T * phi_z

        else:

            x_m = X[ i - 1 ]
            x_z = X[ i ]
            x_p = X[ i + 1 ]

            H_m = H( x_z - x_m )
            phi_m = phi( x_z - x_m, coef_a )
            H_z = H( x_z - x_z )
            phi_z = phi( x_z - x_z, coef_a )
            H_p = H( x_z - x_p )
            phi_p = phi( x_z - x_p,  coef_a )

            M_loc = H_m @ H_m.T * phi_m + H_z @ H_z.T * phi_z + H_p @ H_p.T * phi_p

        #print( f"\nH_m.T: {H_m.T}, phi_m: {phi_m}, H_z.T: {H_z.T}, phi_z: {phi_z}, H_p.T: {H_p.T}, phi_p: {phi_p}\n" )
        #print( f"i: {i}, x[i]: {X[i]}, cond(M): {np.linalg.cond( M_loc )}, M_loc:" )
        #print( M_loc )

        M_inv = np.linalg.inv( M_loc )
        Minv_list[ i, :, : ] = M_inv


    if not implicit_gradient:
        for i in range( 0, n_per_dim + 1 ):

            # Since we assume that particles are quidistant and the kernle size
            # takes into account only neihgboring paritcles, we can directly access the neighbors (left x_m and right x_p)
            if i == 0:

                x_z = X[ i ]
                x_p = X[ i + 1 ]

                H_z = H( x_z - x_z )
                phi_z = phi( x_z - x_z, coef_a )
                dH_z = dH( x_z - x_z )
                dPhi_z = dPhi( x_z - x_z, coef_a )

                H_p = H( x_z - x_p )
                phi_p = phi( x_z - x_p,  coef_a )
                dH_p = dH( x_z - x_p )
                dPhi_p = dPhi( x_z - x_p,  coef_a )

                dM_loc = \
                (dH_z @ H_z.T * phi_z + H_z @ dH_z.T * phi_z + H_z @ H_z.T * dPhi_z) +\
                (dH_p @ H_p.T * phi_p + H_p @ dH_p.T * phi_p + H_p @ H_p.T * dPhi_p)

            elif i == n_per_dim :

                x_m = X[ i - 1 ]
                x_z = X[ i ]

                H_m = H( x_z - x_m )
                phi_m = phi( x_z - x_m, coef_a )
                dH_m = dH( x_z - x_m )
                dPhi_m = dPhi( x_z - x_m, coef_a )

                H_z = H( x_z - x_z )
                phi_z = phi( x_z - x_z, coef_a )
                dH_z = dH( x_z - x_z )
                dPhi_z = dPhi( x_z - x_z, coef_a )

                dM_loc = \
                (dH_m @ H_m.T * phi_m + H_m @ dH_m.T * phi_m + H_m @ H_m.T * dPhi_m) + \
                (dH_z @ H_z.T * phi_z + H_z @ dH_z.T * phi_z + H_z @ H_z.T * dPhi_z)

            else:

                x_m = X[ i - 1 ]
                x_z = X[ i ]
                x_p = X[ i + 1 ]

                H_m = H( x_z - x_m )
                phi_m = phi( x_z - x_m, coef_a )
                dH_m = dH( x_z - x_m )
                dPhi_m = dPhi( x_z - x_m, coef_a )

                H_z = H( x_z - x_z )
                phi_z = phi( x_z - x_z, coef_a )
                dH_z = dH( x_z - x_z )
                dPhi_z = dPhi( x_z - x_z, coef_a )

                H_p = H( x_z - x_p )
                phi_p = phi( x_z - x_p,  coef_a )
                dH_p = dH( x_z - x_p )
                dPhi_p = dPhi( x_z - x_p,  coef_a )

                dM_loc = \
                (dH_m @ H_m.T * phi_m + H_m @ dH_m.T * phi_m + H_m @ H_m.T * dPhi_m) +\
                (dH_z @ H_z.T * phi_z + H_z @ dH_z.T * phi_z + H_z @ H_z.T * dPhi_z) +\
                (dH_p @ H_p.T * phi_p + H_p @ dH_p.T * phi_p + H_p @ H_p.T * dPhi_p)


            M_inv_i = Minv_list[ i ]
            # inverse of the matrix derivative -M^-1*dM/dx*M^-1
            dMinv_list[ i, :, : ] = -M_inv_i @ dM_loc @ M_inv_i

    # System matrix
    K = np.zeros( [ n_per_dim + 1, n_per_dim +1 ] )

    for i in range( 0, n_per_dim + 1 ):
        for j in range( 0, n_per_dim  + 1 ):

            # K[ i, j ] = \int_\[-1, 1] psi_dx[ i ]( x ) psi_dx[ j ]( x )
            Kij = 0

            ## Integrate over the interval [ -1, 1 ]:
            #for k in range( 0, n_per_dim + 1 ):
            #    M_inv_k = Minv_list[ k ]
            #    psi_i = np.dot( (Cdx @ M_inv_k) , H( X[ k ] - X[ i ] ) ) * phi( X[ k ] - X[ i ], coef_a )
            #    psi_j = np.dot( (Cdx @ M_inv_k) , H( X[ k ] - X[ j ] ) ) * phi( X[ k ] - X[ j ], coef_a )
            #    Kij += psi_i * psi_j * dx

            # Integrate over the supports of psi_i
            for k in [ i - 1, i, i + 1 ]:
                if k < 0 or k > n_per_dim:
                    continue

                M_inv_k = Minv_list[ k ]
                dM_inv_k = dMinv_list[ k ]

                if (implicit_gradient):
                    psi_i = np.dot( (Cdx @ M_inv_k) , H( X[ k ] - X[ i ] ) ) * phi( X[ k ] - X[ i ], coef_a )
                    psi_j = np.dot( (Cdx @ M_inv_k) , H( X[ k ] - X[ j ] ) ) * phi( X[ k ] - X[ j ], coef_a )
                    Kij += psi_i * psi_j * dx

                else:
                    psi_i =\
                        np.dot(np.dot(C0, dM_inv_k), H(X[k] - X[i])) * phi(X[k] - X[i], coef_a) +\
                        np.dot(np.dot(C0, M_inv_k), dH(X[k] - X[i])) * phi(X[k] - X[i], coef_a) +\
                        np.dot(np.dot(C0, M_inv_k), H(X[k] - X[i])) * dPhi(X[k] - X[i], coef_a)

                    psi_j =\
                        np.dot(np.dot(C0, dM_inv_k), H(X[k] - X[j])) * phi(X[k] - X[j], coef_a) +\
                        np.dot(np.dot(C0, M_inv_k), dH(X[k] - X[j])) * phi(X[k] - X[j], coef_a) +\
                        np.dot(np.dot(C0, M_inv_k), H(X[k] - X[j])) * dPhi(X[k] - X[j], coef_a)


                    Kij += psi_i * psi_j * dx

            K[ i, j ] = Kij

    #print( f"Matrix K:\n{K}" )
    print( f"Det( K ): { np.linalg.det( K ) }, Cond( K ): { np.linalg.cond( K ) }" )
    plt.imshow( K )
    plt.show()

    Kred = K[ 1 : n_per_dim, 1 : n_per_dim ]
    #print( f"Matrix Kred:\n{ Kred }" )
    print( f"Det( Kred ): { np.linalg.det( Kred ) }, Cond( Kred ): { np.linalg.cond( Kred ) }" )
    plt.imshow( Kred )
    plt.show()

    # Compute external forces (RHS)
    f_r = np.zeros( [ n_per_dim + 1 ] )

    for i in range( 1, n_per_dim ):

        f_r_i = 0
        for k in range( 1, n_per_dim ):

            M_inv_k = Minv_list[ k ]
            psi_i = C0 @ M_inv_k @ H( X[ k ] - X[ i ] ) * phi( X[ k ] - X[ i ], coef_a )
            f_r_i += psi_i * r[ k ]

        f_r[ i ] = f_r_i * dx

    print( "f_r:" )
    print( f_r )

    ## Solve system using in-build tools
    #d_red = np.linalg.solve( Kred, f_r[ 1 : n_per_dim ] )

    ## Solve the system manually using CG method
    r_red = f_r[ 1 : n_per_dim ]
    d_red = np.zeros( [ n_per_dim - 1 ] )

    res = r_red - Kred @ d_red
    p = res.copy()
    res_norm_old = np.dot( res, res )
    print( f"Initial residuum: {res_norm_old}" )

    for i in range( 0, 500 ):
        Ap = Kred @ p
        alpha = res_norm_old/ np.dot( p, Ap )
        d_red += alpha * p
        res -= alpha * Ap
        res_norm_new = np.dot( res, res )
        print( f"New residuum: {res_norm_new}, alpha: {alpha}, ||Ap||_2: {np.linalg.norm( Ap )}" )
        if  np.sqrt( res_norm_new < 1e-12 ):
            print( f'Converged in {i+1} iterations.' )
            break

        beta = res_norm_new / res_norm_old
        p = res + beta * p
        res_norm_old = res_norm_new

    # add the boundary nodes
    d = np.zeros( n_per_dim + 1 )
    d[ 1 : n_per_dim ] = d_red

    # Compute the field from nodal values
    u = np.zeros( [ n_per_dim + 1 ] )
    for i in range( 1, n_per_dim ):

        u_i = 0
        #for k in [ i - 1, i, i + 1 ]:
        #    if k < 0 or k > n_per_dim:
        #        continue
        #    M_inv_k = Minv_list[ k ]
        #    psi_i = C0 @ M_inv_k @ H( X[ k ] - X[ i ] ) * phi( X[ k ] - X[ i ], coef_a )
        #    u_i += psi_i * d[ k ] # * dx  #this is absolutely misleading notation
        #
        #u[ i ] = u_i

        M_inv_i = Minv_list[ i ]
        psi_i_m = C0 @ M_inv_i @ H( X[ i ] - X[ i - 1 ] ) * phi( X[ i ] - X[ i -1 ], coef_a )
        psi_i_z = C0 @ M_inv_i @ H( X[ i ] - X[ i ] ) * phi( X[ i ] - X[ i ], coef_a )
        psi_i_p = C0 @ M_inv_i @ H( X[ i ] - X[ i  + 1 ] ) * phi( X[ i ] - X[ i + 1 ], coef_a )

        u[ i ] = psi_i_m * d[ i - 1 ] + psi_i_z * d[ i ] + psi_i_p * d[ i + 1 ]

    # Analytical solution
    u_exact = lambda x : 1 - x**2

    # Plot parameters
    plt.rcParams.update({
      "text.usetex": True,
      "text.latex.preamble" : r"\usepackage{amsfonts}",
      "font.family": "Times",
      "font.serif" : "Times New Roman",
      "font.size"  : 14,
    })

    fig = plt.figure( figsize=(15,6) )
    ax = fig.add_subplot( 1, 2, 1 )
    plt.plot( X, u, "r", marker="o", label=r"$u(x_i)$ num." )
    plt.plot( X, u_exact( X ), "--k", label=r"$u(x)$ exact" )
    plt.grid( color='gray', linestyle='dashed' )
    plt.legend( fontsize=16 )
    plt.title( "Interpolated solution", fontsize=18 )

    ax = fig.add_subplot( 1, 2, 2 )
    plt.scatter( X, d, color="b", label=r"$d_i$ - nodal vals." )
    plt.grid( color='gray', linestyle='dashed' )
    plt.legend( fontsize=16 )
    plt.title( "Nodal values", fontsize=18 )

    plt.savefig("results_1d_rkpm.png", bbox_inches='tight')
