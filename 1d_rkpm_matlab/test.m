n_per_dim_extended = 64;
n_per_dim = n_per_dim_extended - 2;
Lx = 2;
lex = 4 / ( n_per_dim_extended - 2 );
x_begin = -1;
x_end = x_begin + Lx + lex;
x_end_extended = x_begin + Lx + lex;
dx = ( Lx + lex ) / n_per_dim_extended;

% X=x_begin:dx:x_end;
X=x_begin:dx:x_begin + Lx;


% kernel support size coef
coef_a_const = 1.2;

% kernel support size
coef_a = coef_a_const * dx;
dni_volume = dx;

order=1;

Shapes=RKShape.empty(size(X,2), 0);
for i=1:size(X,2)
   Shapes(i)=RKShape(i, X, coef_a, order); 
end

%% Test Partion of Unity:
% pu=0;
% x=rand()*1.5;
% for i=1:size(X,2)
%     pu=pu+Shapes(i).getValue(x);
% end
% assert(abs(pu-1.0)<5*eps)

%% Develop Mesh:
quadOrder = 1;

% quadrature rule which reproduces the behaviour of the python version (+ change line 24 in Element.m)
% Q =QuadMesh(x_begin, x_begin + Lx + dx, n_per_dim + 1, quadOrder);

% midpoint integration that fixes the issue
Q =QuadMesh(x_begin, x_begin + Lx, n_per_dim, quadOrder);
Q.setElements();


%% Develop Matrices:
n=length(X);
K=zeros(n);
F=zeros(n,1);
r = 2*ones(n+2);
for k=1:Q.noElements % Loop over Integration zones
    for l=1:Q.quadrature % Loop over integration point
        intCordinate=Q.Elements(k).getGlobalX(Q.Elements(k).qPoints(l,1));
        intWeight=Q.Elements(k).qPoints(l,2);
        intJacobian=Q.Elements(k).jV;
        for i=1:n % Loop over Shape Functions
            for j=1:n % Loop over Shape Functions
                K(i,j)=K(i,j)+Shapes(i).getValueDx(intCordinate)*Shapes(j).getValueDx(intCordinate)*intWeight*intJacobian;
            end
            F(i)=F(i)+Shapes(i).getValue(intCordinate)*intWeight*intJacobian*r(i);
        end
    end
end

%% Solve System:
uUnknown=ones(n,1)*-inf;  uUnknown(1)=0; uUnknown(n)=0; logical=(uUnknown==-inf);
Kr=K(logical,logical); Fr=F(logical);
cond(Kr)
dr=Kr\Fr;
d=[0;dr;0];

%% Interpolate the Solution
Result=zeros(length(X),1);
for g=1:length(X)
    x=X(g);
    for i=1:n
       Result(g)=Result(g)+Shapes(i).getValue(x)*d(i);
    end
end
plot(X,Result);
