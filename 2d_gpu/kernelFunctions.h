namespace kernels {

__cuda_callable__
float
phi_core( const float r, const float h )
{
   const float z =  TNL::abs( r ) / h;

   if( ( 0 <= z ) && ( z < 0.5 ) )
       return 2.f / 3.f  - 4.f * z * z + 4.f * z * z * z;
   else if( ( z >= 0.5f ) && ( z < 1.f ) )
       return 4.f / 3.f - 4.f * z + 4.f * z * z - 4.f / 3.f * z * z;
   else
       return 0.f;
}

__cuda_callable__
float
phi( const float x, const float y, const float hx, const float hy )
{
   return phi_core( x, hx ) * phi_core( y, hy );
}

}

