#include <TNL/Devices/Cuda.h>

#include "fcbm.h"

using RealType = float;
using Device = TNL::Devices::Cuda;
static constexpr int nx = 8;
static constexpr int ny = 8;
//using SizeHolder = TNL::Containers::SizesHolder< int, 8, 8 >;


int main()
{
   Fcbm< RealType, Device > solver;

   //solver.initialize();
   //solver.execute();

   solver.initializeStorage();
   solver.initializeDomainVectors();
   solver.initializeRHS();
   solver.initializeBaseVectors();
   solver.initializeMomentumConditions();
   solver.evalExternalForces();
   solver.evalInternalForces( solver.d );

   solver.solveWithConjugateGradients();

   std::cout << "Done..." << std::endl;
   return EXIT_SUCCESS;
}

