#include <iostream>
#include <complex>
#include <TNL/Containers/Array.h>
#include <TNL/Containers/StaticVector.h>
#include <TNL/Devices/Cuda.h>
#include <cufft.h>
#include "cufft_utils.h"

//temp
#include <array>

int main(){

   using Device = TNL::Devices::Cuda;
   using RealType = float;
   //using ComplexType = std::complex< RealType >;
   using ComplexType = TNL::Containers::StaticVector< 2, float >;
   //using DimType = TNL::Containers::StaticVector< 2, int >;
   using DimType = std::array< int, 2 >;

   DimType fft_size = { 16 , 16 };
   int fft_elements_size = fft_size[ 0 ] * fft_size[ 1 ];
   int batch_size = 2;

   TNL::Containers::Array< RealType, Device > inputVect( fft_elements_size  );
   inputVect.forAllElements( [] __cuda_callable__( int i, RealType& val ) { val = i; } );

   TNL::Containers::Array< ComplexType, Device > outputVect( fft_size[ 0 ] * ( fft_size[ 1 ] / 2 + 1 ) * batch_size );
   outputVect = 0;

   std::cout << "Initialization:" << std::endl;
   std::cout << "Input vect:\n" << inputVect << std::endl;
   std::cout << "Output vect:\n" << outputVect << std::endl;

   cufftHandle planr2c, planc2r;
   cudaStream_t stream = NULL;
   CUFFT_CALL( cufftCreate( &planr2c ) );
   CUFFT_CALL( cufftCreate( &planc2r ) );
   //CUFFT_CALL( cufftPlan2d( &planr2c, fft_size[ 0 ], fft_size[ 1 ], CUFFT_R2C ) ); // cufftPlan2d doesn't include batch_size
   //CUFFT_CALL( cufftPlan2d( &planc2r, fft_size[ 0 ], fft_size[ 1 ], CUFFT_C2R ) ); // cufftPlan2d doesn't include batch_size

   // NOTE: Idk why this hates DimType = TNL::Containers::StaticVector< 2, int >, getSize() and getData()
   //CUFFT_CALL( cufftPlanMany( &planr2c, fft_size.getSize(), fft_size.getData(), nullptr, 1, 0, CUFFT_R2C, batch_size ) );
   //CUFFT_CALL( cufftPlanMany( &planr2c, fft_size.getSize(), fft_size.getData(), nullptr, 1, 0, CUFFT_C2R, batch_size ) );
   CUFFT_CALL( cufftPlanMany( &planr2c, fft_size.size(), fft_size.data(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
   CUFFT_CALL( cufftPlanMany( &planr2c, fft_size.size(), fft_size.data(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );

   CUDA_RT_CALL(cudaStreamCreateWithFlags( &stream, cudaStreamNonBlocking ) );
   CUFFT_CALL( cufftSetStream( planr2c, stream ) );
   CUFFT_CALL( cufftSetStream( planc2r, stream ) );

   // out-of-place Forward transform
   // NOTE: cufftComplex is just float2
   CUFFT_CALL( cufftExecR2C( planr2c, inputVect.getData(), reinterpret_cast< cufftComplex* >( outputVect.getData() ) ) );

   std::cout << "Results after ForwardFFT:" << std::endl;
   std::cout << "Input vect:\n" << inputVect << std::endl;
   std::cout << "Output vect:\n" << outputVect << std::endl;

   // normalize the data
   float sizeScale = ( 1.f / fft_elements_size );
   outputVect.forAllElements( [=] __cuda_callable__( int i, ComplexType& val ) { val *= sizeScale; } );

   // out-of-place Inverse transform
   TNL::Containers::Array< RealType, Device > inversionVect( fft_elements_size );
   inversionVect = 0.;

   CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex * >( outputVect.getData() ) , inversionVect.getData() ) );

   std::cout << "Results after Forward FFT, normalization, Inverse FFT:" << std::endl;
   std::cout << "Input vect:\n" << inputVect << std::endl;
   std::cout << "Output vect:\n" << outputVect << std::endl;
   std::cout << "Inversion vect:\n" << inputVect << std::endl;

   std::cout << "Done..." << std::endl;
   return EXIT_SUCCESS;
}

