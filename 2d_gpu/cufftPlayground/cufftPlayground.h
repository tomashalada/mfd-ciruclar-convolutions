
#include <iostream>
#include <complex>
#include <TNL/Containers/Array.h>
#include <TNL/Containers/StaticVector.h>
#include <TNL/Devices/Cuda.h>
#include <cufft.h>
#include "cufft_utils.h"

int main(){

   using Device = TNL::Devices::Cuda;
   using RealType = float;
   //using ComplexType = std::complex< RealType >;
   using ComplexType = TNL::Containers::StaticVector< 2, float >;
   int fft_size = 64;
   int batch_size = 2;

   TNL::Containers::Array< RealType, Device > inputVect( fft_size );
   inputVect.forAllElements( [] __cuda_callable__( int i, RealType& val ) { val = i; } );

   TNL::Containers::Array< ComplexType, Device > outputVect( ( fft_size / 2 + 1 ) * batch_size );
   //TNL::Containers::Array< RealType, Device > outputVect( ( fft_size / 2 + 1 ) * batch_size );
   outputVect = 0;

   std::cout << "Initialization:" << std::endl;
   std::cout << "Input vect:\n" << inputVect << std::endl;
   std::cout << "Output vect:\n" << outputVect << std::endl;

   cufftHandle planr2c, planc2r;
   cudaStream_t stream = NULL;
   CUFFT_CALL( cufftCreate( &planr2c ) );
   CUFFT_CALL( cufftCreate( &planc2r ) );
   CUFFT_CALL( cufftPlan1d( &planr2c, fft_size, CUFFT_R2C, batch_size ) );
   CUFFT_CALL( cufftPlan1d( &planc2r, fft_size, CUFFT_C2R, batch_size ) );

   CUDA_RT_CALL(cudaStreamCreateWithFlags( &stream, cudaStreamNonBlocking ) );
   CUFFT_CALL( cufftSetStream( planr2c, stream ) );
   CUFFT_CALL( cufftSetStream( planc2r, stream ) );

   // out-of-place Forward transform
   // NOTE: cufftComplex is just float2
   CUFFT_CALL( cufftExecR2C( planr2c, inputVect.getData(), reinterpret_cast< cufftComplex * >( outputVect.getData() ) ) );
   CUDA_RT_CALL(cudaStreamSynchronize(stream));

   std::cout << "Results after ForwardFFT:" << std::endl;
   std::cout << "Input vect:\n" << inputVect << std::endl;
   std::cout << "Output vect:\n" << outputVect << std::endl;

   // normalize the data
   float sizeScale = ( 1.f / fft_size );
   outputVect.forAllElements( [=] __cuda_callable__( int i, ComplexType& val ) { val *= sizeScale; } );

   // out-of-place Inverse transform
   TNL::Containers::Array< RealType, Device > inversionVect( fft_size );
   inversionVect = 0.;

   CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex * >( outputVect.getData() ) , inversionVect.getData() ) );

   std::cout << "Results after Forward FFT, normalization, Inverse FFT:" << std::endl;
   std::cout << "Input vect:\n" << inputVect << std::endl;
   std::cout << "Output vect:\n" << outputVect << std::endl;
   std::cout << "Inversion vect:\n" << inputVect << std::endl;

   //CUDA_RT_CALL(cudaStreamDestroy(stream));

   //CUDA_RT_CALL(cudaDeviceReset());

   std::cout << "Done..." << std::endl;
   return EXIT_SUCCESS;
}

