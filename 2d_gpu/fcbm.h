#include <TNL/Arithmetics/Complex.h>
#include <TNL/Containers/StaticVector.h>
#include <TNL/Containers/Array.h>
#include <TNL/Containers/Vector.h>
#include <TNL/Containers/NDArray.h>
#include <TNL/Matrices/StaticMatrix.h>
#include <TNL/Algorithms/parallelFor.h>

#include <cufft.h>
#include <random>
#include "cufft_utils.h"

#include "utils.h"
#include "kernelFunctions.h"

template< typename Real, typename Device, typename SizeHolder >
struct DefaultArrayTypes
{
   using DeviceType = Device;
   using RealType = Real;
   using ComplexType = TNL::Containers::StaticVector< 2, RealType >;
   using IdxVect = TNL::Containers::StaticVector< 2, int >;
   using RealVect = TNL::Containers::StaticVector< 2, RealType >;

   using NDArrayReal = TNL::Containers::NDArray< RealType,
                                                 SizeHolder,
                                                 std::index_sequence< 0, 1 >,
                                                 DeviceType >;

   using NDArrayComplex = TNL::Containers::NDArray< ComplexType,
                                                    SizeHolder,
                                                    std::index_sequence< 0, 1 >,
                                                    DeviceType >;
};


template< typename Real, typename Device >
class Fcbm
{
public:

   //using Types = DefaultArrayTypes< Real, Device, SizeHolder >;

   using DeviceType = Device;
   using RealType = Real;
   //using ComplexType = TNL::Containers::StaticVector< 2, RealType >;
   using ComplexType = TNL::Arithmetics::Complex< RealType >;
   using IdxVect = TNL::Containers::StaticVector< 2, int >;
   using RealVect = TNL::Containers::StaticVector< 2, RealType >;

   using ArrayComplex = TNL::Containers::Vector< ComplexType, Device >;
   using ArrayReal = TNL::Containers::Vector< RealType, Device >;

   using SizeHolderT = TNL::Containers::SizesHolder< int, 8, 8 >;
   using NDArrayReal = TNL::Containers::NDArray< RealType,
                                                 SizeHolderT,
                                                 std::index_sequence< 0, 1 >,
                                                 DeviceType >;

   using NDArrayComplex = TNL::Containers::NDArray< ComplexType,
                                                    SizeHolderT,
                                                    std::index_sequence< 0, 1 >,
                                                    DeviceType >;

   Fcbm() = default;

   void
   initializeStorage()
   {

      u.setSizes( gridDims.x(), gridDims.y() );
      d.setSizes( gridDims.x(), gridDims.y() );
      u.getStorageArray() = 0.f;
      d.getStorageArray() = 0.f;

      x.setSizes( gridDims.x(), gridDims.y() );
      y.setSizes( gridDims.x(), gridDims.y() );
      chi.setSizes( gridDims.x(), gridDims.y() );
      chi_g.setSizes( gridDims.x(), gridDims.y() );

      // - class base
      // -- this is basically only part of the initialization
      h1.setSizes( gridDims.x(), gridDims.y() );
      h2.setSizes( gridDims.x(), gridDims.y() );
      h3.setSizes( gridDims.x(), gridDims.y() );

      h1a.setSizes( gridDims.x(), gridDims.y() );
      h2a.setSizes( gridDims.x(), gridDims.y() );
      h3a.setSizes( gridDims.x(), gridDims.y() );

      h1a_bar.setSizes( gridDims.x(), gridDims.y() );
      h2a_bar.setSizes( gridDims.x(), gridDims.y() );
      h3a_bar.setSizes( gridDims.x(), gridDims.y() );

      const int r2c_output_size = gridDims[ 0 ] * ( gridDims[ 1 ] / 2 + 1 );
      h1a_hat.setSize( r2c_output_size );
      h2a_hat.setSize( r2c_output_size );
      h3a_hat.setSize( r2c_output_size );

      h1a_bar_hat.setSize( r2c_output_size );
      h2a_bar_hat.setSize( r2c_output_size );
      h3a_bar_hat.setSize( r2c_output_size );

      // - class base
      // -- this is alsto the only part of the initialization
      b01.setSizes( gridDims.x(), gridDims.y() );
      b02.setSizes( gridDims.x(), gridDims.y() );
      b03.setSizes( gridDims.x(), gridDims.y() );

      bx1.setSizes( gridDims.x(), gridDims.y() );
      bx2.setSizes( gridDims.x(), gridDims.y() );
      bx3.setSizes( gridDims.x(), gridDims.y() );

      by1.setSizes( gridDims.x(), gridDims.y() );
      by2.setSizes( gridDims.x(), gridDims.y() );
      by3.setSizes( gridDims.x(), gridDims.y() );

      // - class related to evaluation of internal forces
      // -- this is required by eval force, uses temp arrays
      d_chi_hat.setSize( r2c_output_size );

      d_chi_hat_h1a_hat.setSize( r2c_output_size );
      d_chi_hat_h2a_hat.setSize( r2c_output_size );
      d_chi_hat_h3a_hat.setSize( r2c_output_size );

      evalIntForce_D1.setSizes( gridDims.x(), gridDims.y() );
      evalIntForce_D2.setSizes( gridDims.x(), gridDims.y() );
      evalIntForce_D3.setSizes( gridDims.x(), gridDims.y() );

      ax.setSizes( gridDims.x(), gridDims.y() );
      ay.setSizes( gridDims.x(), gridDims.y() );

      ax_cx1_ay_cy1_sum.setSizes( gridDims.x(), gridDims.y() );
      ax_cx2_ay_cy2_sum.setSizes( gridDims.x(), gridDims.y() );
      ax_cx3_ay_cy3_sum.setSizes( gridDims.x(), gridDims.y() );

      ax_cx1_ay_cy1_sum_hat.setSize( r2c_output_size );
      ax_cx2_ay_cy2_sum_hat.setSize( r2c_output_size );
      ax_cx3_ay_cy3_sum_hat.setSize( r2c_output_size );

      b_hat.setSize( r2c_output_size );

      f_int.setSizes( gridDims.x(), gridDims.y() );
      f_int_vect.setSize( gridDims.x() * gridDims.y() );

      // - class related to evaluation of external forces
      // -- this is required by eval force, uses temp arrays
      f_ext.setSizes( gridDims.x(), gridDims.y() );
      f_ext_vect.setSize( gridDims.x() * gridDims.y() );

      rhs.setSizes( gridDims.x(), gridDims.y() );

   }

   void
   initializeDomainVectors()
   {
      // generate coordinates
      TNL::Containers::Vector< RealType, Device > x_coords( gridDims.x() ), y_coords( gridDims.y() );

      const RealType x_begin = domainOrigin.x();
      const RealType y_begin = domainOrigin.y();
      const RealType dx = stepSizes.x();
      const RealType dy = stepSizes.y();

      x_coords.forAllElements( [=] __cuda_callable__( int i, RealType& val ) { val = x_begin + i * dx; } );
      y_coords.forAllElements( [=] __cuda_callable__( int i, RealType& val ) { val = y_begin + i * dy; } );
      // eval domain size:
      domainSizesWithExtensions = { x_coords.getElement( gridDims.x() - 1 ) - x_coords.getElement( 0 ), y_coords.getElement( gridDims.y() - 1 ) - y_coords.getElement( 0 ) };


      auto chi_view = chi.getView();
      auto chi_g_view = chi_g.getView();
      auto x_view = x.getView();
      auto y_view = y.getView();
      const auto x_coords_view = x_coords.getConstView();
      const auto y_coords_view = y_coords.getConstView();

      const RealType dx_eps = 0.25 * stepSizes.x();
      const RealType dy_eps = 0.25 * stepSizes.y();

      // transform coordinates to meshgrid, domain characteristic functions
      auto set_domain = [=] __cuda_callable__ ( const IdxVect& idx ) mutable
      {
         const int i = idx.x();
         const int j = idx.y();

         const RealType x_loc = x_coords_view[ j ];
         const RealType y_loc = y_coords_view[ i ];
         x_view( i, j ) = x_loc;
         y_view( i, j ) = y_loc;

         //chi vector
         if( x_loc < -1.0f  || x_loc > 1.0f || y_loc < -1.0f || y_loc > 1.0f )
            chi_view( i, j ) = 0.f;
         else
            chi_view( i, j ) = 1.f;

         //chi_g vector
         chi_g_view( i, j ) = 0;
         if ( ( ( x_loc > ( -1.0 - dx_eps ) ) && ( x_loc < ( -1.0 + dy_eps ) ) && ( y_loc < 1 + dy_eps ) ) || ( ( ( x_loc < ( 1.0 + dx_eps ) ) && ( x_loc > ( 1.0 - dx_eps ) ) ) && ( y_loc < 1 + dy_eps ) ) )
            chi_g_view( i, j ) = 1;
         if ( ( ( y_loc > ( -1.0 - dy_eps ) ) && ( y_loc < ( -1.0 + dy_eps ) ) && ( x_loc < 1 + dx_eps ) ) || ( ( ( y_loc < ( 1.0 + dy_eps ) ) && ( y_loc > ( 1.0 - dy_eps ) ) ) && ( x_loc < 1 + dx_eps ) ) )
            chi_g_view( i, j ) = 1;
      };
      IdxVect begin = { 0, 0 }, end = { gridDims.x(), gridDims.y() };
      TNL::Algorithms::parallelFor< Device >( begin, end, set_domain );

      // debug
      utils::dumpArrayToFile( chi, "results/field_chi.tnl");
      utils::dumpArrayToFile( chi_g, "results/field_chi_g.tnl");
      utils::dumpArrayToFile( x, "results/field_x.tnl");
      utils::dumpArrayToFile( y, "results/field_y.tnl");
   }

   void
   initializeBaseVectors()
   {
      auto h1_view = h1.getView();
      auto h2_view = h2.getView();
      auto h3_view = h3.getView();

      auto h1a_view = h1a.getView();
      auto h2a_view = h2a.getView();
      auto h3a_view = h3a.getView();

      auto h1a_bar_view = h1a_bar.getView();
      auto h2a_bar_view = h2a_bar.getView();
      auto h3a_bar_view = h3a_bar.getView();

      const auto x_view = x.getConstView();
      const auto y_view = y.getConstView();

      const RealVect domainCenter = domainOrigin + 0.5f * ( domainSizesWithExtensions + stepSizes );
      const RealVect kernelWidth = this->kernelWidth;
      auto set_h_vectors = [=] __cuda_callable__ ( const IdxVect& idx ) mutable
      {
         const int i = idx[ 0 ];
         const int j = idx[ 1 ];

         const RealType x_ij_c = x_view( i, j ) - domainCenter.x();
         const RealType y_ij_c = y_view( i, j ) - domainCenter.y();
         const RealType phi_ij_c = kernels::phi( x_ij_c, y_ij_c , kernelWidth.x(), kernelWidth.y() );
         const RealType phi_m_ij_c = kernels::phi( ( -1.f ) * x_ij_c, ( -1.f ) * y_ij_c , kernelWidth.x(), kernelWidth.y() );

         h1_view( i, j ) = 1.f;
         h2_view( i, j ) = x_ij_c;
         h3_view( i, j ) = y_ij_c;

         h1a_view( i, j ) = phi_ij_c;
         h2a_view( i, j ) = x_ij_c * phi_ij_c;
         h3a_view( i, j ) = y_ij_c * phi_ij_c;

         h1a_bar_view( i, j ) = phi_m_ij_c;
         h2a_bar_view( i, j ) = ( -1.f ) * x_ij_c * phi_m_ij_c;
         h3a_bar_view( i, j ) = ( -1.f ) * y_ij_c * phi_m_ij_c;
      };
      IdxVect begin = { 0, 0 }, end = { gridDims.x(), gridDims.y() };
      TNL::Algorithms::parallelFor< Device >( begin, end, set_h_vectors );

      utils::fftshift( h1, gridDims.x(), gridDims.y() );
      utils::fftshift( h2, gridDims.x(), gridDims.y() );
      utils::fftshift( h3, gridDims.x(), gridDims.y() );

      utils::fftshift( h1a, gridDims.x(), gridDims.y() );
      utils::fftshift( h2a, gridDims.x(), gridDims.y() );
      utils::fftshift( h3a, gridDims.x(), gridDims.y() );

      utils::fftshift( h1a_bar, gridDims.x(), gridDims.y() );
      utils::fftshift( h2a_bar, gridDims.x(), gridDims.y() );
      utils::fftshift( h3a_bar, gridDims.x(), gridDims.y() );

      // debug
      utils::dumpArrayToFile( h1, "results/field_h1.tnl");
      utils::dumpArrayToFile( h2, "results/field_h2.tnl");
      utils::dumpArrayToFile( h3, "results/field_h3.tnl");

      utils::dumpArrayToFile( h1a, "results/field_h1a.tnl");
      utils::dumpArrayToFile( h2a, "results/field_h2a.tnl");
      utils::dumpArrayToFile( h3a, "results/field_h3a.tnl");

      utils::dumpArrayToFile( h1a_bar, "results/field_h1a_bar.tnl");
      utils::dumpArrayToFile( h2a_bar, "results/field_h2a_bar.tnl");
      utils::dumpArrayToFile( h3a_bar, "results/field_h3a_bar.tnl");

      // compute the ffts of the base vectors
      cufftHandle planr2c;
      cudaStream_t stream = NULL;
      const int batch_size = 1; // NOTE: All these operations should be batched
      CUFFT_CALL( cufftCreate( &planr2c ) );

      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );

      CUDA_RT_CALL(cudaStreamCreateWithFlags( &stream, cudaStreamNonBlocking ) );
      CUFFT_CALL( cufftSetStream( planr2c, stream ) );

      CUFFT_CALL( cufftExecR2C( planr2c, h1a.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h1a_hat.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, h2a.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h2a_hat.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, h3a.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h3a_hat.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, h1a_bar.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h1a_bar_hat.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, h2a_bar.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h2a_bar_hat.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, h3a_bar.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h3a_bar_hat.getData() ) ) );


   }

   void
   initializeRHS()
   {

      const auto x_view = x.getConstView();
      const auto y_view = y.getConstView();
      auto rhs_view = rhs.getView();

      auto set_h_vectors = [=] __cuda_callable__ ( const IdxVect& idx ) mutable
      {
         const int i = idx[ 0 ];
         const int j = idx[ 1 ];
         rhs_view( i, j ) = defineRhs( x_view( i , j ), y_view( i, j ) );
      };
      IdxVect begin = { 0, 0 }, end = { gridDims.x(), gridDims.y() };
      TNL::Algorithms::parallelFor< Device >( begin, end, set_h_vectors );

      // debug
      utils::dumpArrayToFile( rhs, "results/field_rhs.tnl" );
   }

   void
   initializeMomentumConditions()
   {
      // define temp arrays to build momentum conditions
      NDArrayReal m11;
      NDArrayReal m12;
      NDArrayReal m13;
      // m21 = m21
      NDArrayReal m22;
      NDArrayReal m23;
      // m31 = m13
      // m32 = m23
      NDArrayReal m33;
      m11.setSizes( gridDims.x(), gridDims.y() );
      m12.setSizes( gridDims.x(), gridDims.y() );
      m13.setSizes( gridDims.x(), gridDims.y() );
      m22.setSizes( gridDims.x(), gridDims.y() );
      m23.setSizes( gridDims.x(), gridDims.y() );
      m33.setSizes( gridDims.x(), gridDims.y() );

      // define temm arrays for the ffts;
      NDArrayReal h1_h1a_fft_in;
      NDArrayReal h1_h2a_fft_in;
      NDArrayReal h1_h3a_fft_in;
      // h2_h1a = h1_h2a
      NDArrayReal h2_h2a_fft_in;
      NDArrayReal h2_h3a_fft_in;
      // h3_h1a = h1_h3a
      // h3_h2a = h2_h3a
      NDArrayReal h3_h3a_fft_in;
      h1_h1a_fft_in.setSizes( gridDims.x(), gridDims.y() );
      h1_h2a_fft_in.setSizes( gridDims.x(), gridDims.y() );
      h1_h3a_fft_in.setSizes( gridDims.x(), gridDims.y() );
      h2_h2a_fft_in.setSizes( gridDims.x(), gridDims.y() );
      h2_h3a_fft_in.setSizes( gridDims.x(), gridDims.y() );
      h3_h3a_fft_in.setSizes( gridDims.x(), gridDims.y() );

      // define temm arrays for the fft output;
      const int r2c_output_size = gridDims[ 0 ] * ( gridDims[ 1 ] / 2 + 1 );

      ArrayComplex chi_hat_fft_out;
      chi_hat_fft_out.setSize( r2c_output_size );

      ArrayComplex h1_h1a_chi_fft_out;
      ArrayComplex h1_h2a_chi_fft_out;
      ArrayComplex h1_h3a_chi_fft_out;
      // h2_h1a = h1_h2a
      ArrayComplex h2_h2a_chi_fft_out;
      ArrayComplex h2_h3a_chi_fft_out;
      // h3_h1a = h1_h3a
      // h3_h2a = h2_h3a
      ArrayComplex h3_h3a_chi_fft_out;
      h1_h1a_chi_fft_out.setSize( r2c_output_size );
      h1_h2a_chi_fft_out.setSize( r2c_output_size );
      h1_h3a_chi_fft_out.setSize( r2c_output_size );
      h2_h2a_chi_fft_out.setSize( r2c_output_size );
      h2_h3a_chi_fft_out.setSize( r2c_output_size );
      h3_h3a_chi_fft_out.setSize( r2c_output_size );

      //NDArrayComplex h1_h1a_chi_fft_out;
      //NDArrayComplex h1_h2a_chi_fft_out;
      //NDArrayComplex h1_h3a_chi_fft_out;
      //// h2_h1a = h1_h2a_chi
      //NDArrayComplex h2_h2a_chi_fft_out;
      //NDArrayComplex h2_h3a_chi_fft_out;
      //// h3_h1a = h1_h3a
      //// h3_h2a = h2_h3a
      //NDArrayComplex h3_h3a_chi_fft_out;
      //h1_h1a_chi_fft_out.setSizes( gridDims.x(), gridDims.y() );
      //h1_h2a_chi_fft_out.setSizes( gridDims.x(), gridDims.y() );
      //h1_h3a_chi_fft_out.setSizes( gridDims.x(), gridDims.y() );
      //h2_h2a_chi_fft_out.setSizes( gridDims.x(), gridDims.y() );
      //h2_h3a_chi_fft_out.setSizes( gridDims.x(), gridDims.y() );
      //h3_h3a_chi_fft_out.setSizes( gridDims.x(), gridDims.y() );

      // fill the input fields
      // NOTE: This would work with NDArray storage array Containers::Vector, which allows to use expression templates
      //h1_h1a_fft_in.getStorageArray() = h1.getStorageArray() * h1a.getStorageArray();
      //h1_h2a_fft_in.getStorageArray() = h1.getStorageArray() * h2a.getStorageArray();
      //h1_h3a_fft_in.getStorageArray() = h1.getStorageArray() * h3a.getStorageArray();
      //// h2_h1a = h1_h2a
      //h2_h2a_fft_in.getStorageArray() = h2.getStorageArray() * h2a.getStorageArray();
      //h2_h3a_fft_in.getStorageArray() = h2.getStorageArray() * h3a.getStorageArray();
      //// h3_h1a = h1_h3a
      //// h3_h2a = h2_h3a
      //h3_h3a_fft_in.getStorageArray() = h3.getStorageArray() * h3a.getStorageArray();

      // NOTE: Doing this in single kernel should be also faster
      utils::multiplyNDArrays( h1_h1a_fft_in.getView(), h1.getView(), h1a.getView() );
      utils::multiplyNDArrays( h1_h2a_fft_in.getView(), h1.getView(), h2a.getView() );
      utils::multiplyNDArrays( h1_h3a_fft_in.getView(), h1.getView(), h3a.getView() );
      // h2_h1a = h1_h2a
      utils::multiplyNDArrays( h2_h2a_fft_in.getView(), h2.getView(), h2a.getView() );
      utils::multiplyNDArrays( h2_h3a_fft_in.getView(), h2.getView(), h3a.getView() );
      // h3_h1a = h1_h3a
      // h3_h2a = h2_h3a
      utils::multiplyNDArrays( h3_h3a_fft_in.getView(), h3.getView(), h3a.getView() );


      //auto h1_h1a_fft_in_view = h1_h1a_fft_in.getView();
      //auto h1_h2a_fft_in_view = h1_h2a_fft_in.getView();
      //auto h1_h3a_fft_in_view = h1_h3a_fft_in.getView();
      //auto h2_h2a_fft_in_view = h2_h2a_fft_in.getView();
      //auto h2_h3a_fft_in_view = h2_h3a_fft_in.getView();
      //auto h3_h3a_fft_in_view = h3_h3a_fft_in.getView();

      // build the products entering ffts
      //auto hp_hq_products = [=] __cuda_callable__ ( const IdxVect& idx ) mutable
      //{
      //   const int i = idx.x();
      //   const int j = idx.y();

      //   h1_h1a_fft_in_view( i, j ) =
      //   h1_h2a_fft_in_view( i, j ) =
      //   h1_h3a_fft_in_view( i, j ) =
      //   h2_h2a_fft_in_view( i, j ) =
      //   h2_h3a_fft_in_view( i, j ) =
      //   h3_h3a_fft_in_view( i, j ) =
      //};
      //IdxVect begin = { 0, 0 }, end = { gridDims.x(), gridDims.y() };
      //TNL::Algorithms::parallelFor< Device >( begin, end, hphq_products );

      // perform the forward ffts:
      cufftHandle planr2c, planc2r;
      cudaStream_t stream = NULL;
      const int batch_size = 1; // NOTE: All these operations should be batched
      CUFFT_CALL( cufftCreate( &planr2c ) );
      CUFFT_CALL( cufftCreate( &planc2r ) );

      // chi
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );

      // base vectors products forward fft
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );

      // base vector procut inverse ffts
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );

      CUDA_RT_CALL(cudaStreamCreateWithFlags( &stream, cudaStreamNonBlocking ) );
      CUFFT_CALL( cufftSetStream( planr2c, stream ) );
      CUFFT_CALL( cufftSetStream( planc2r, stream ) );

      // perform the forward ffts
      // characterostoc function
      CUFFT_CALL( cufftExecR2C( planr2c, chi.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( chi_hat_fft_out.getData() ) ) );

      //std::cout << std::setprecision(2);
      std::cout << "h1: " << h1.getStorageArray() << std::endl << std::endl;
      std::cout << "h1a: " << h1a.getStorageArray() << std::endl << std::endl;
      std::cout << "h1_h1a_fft_in: " << h1_h1a_fft_in.getStorageArray() << std::endl << std::endl;

      // basis vectors
      CUFFT_CALL( cufftExecR2C( planr2c, h1_h1a_fft_in.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h1_h1a_chi_fft_out.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, h1_h2a_fft_in.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h1_h2a_chi_fft_out.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, h1_h3a_fft_in.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h1_h3a_chi_fft_out.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, h2_h2a_fft_in.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h2_h2a_chi_fft_out.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, h2_h3a_fft_in.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h2_h3a_chi_fft_out.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, h3_h3a_fft_in.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( h3_h3a_chi_fft_out.getData() ) ) );

      std::cout << "rffted: h1_h1a: " << h1_h1a_chi_fft_out << std::endl << std::endl;
      std::cout << "chi_hat_fft_out: " << chi_hat_fft_out << std::endl << std::endl;

      // cufft doesn't include normalization
      const int pointsCount = gridDims.x() * gridDims.y();

      h1_h1a_chi_fft_out *= chi_hat_fft_out / pointsCount;
      h1_h2a_chi_fft_out *= chi_hat_fft_out / pointsCount;
      h1_h3a_chi_fft_out *= chi_hat_fft_out / pointsCount;
      h2_h2a_chi_fft_out *= chi_hat_fft_out / pointsCount;
      h2_h3a_chi_fft_out *= chi_hat_fft_out / pointsCount;
      h3_h3a_chi_fft_out *= chi_hat_fft_out / pointsCount;

      std::cout << "ha_h1a * chi_hat_fft_out: " << h1_h1a_chi_fft_out << std::endl << std::endl;

      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( h1_h1a_chi_fft_out.getData() ), h1_h1a_fft_in.getStorageArray().getData() ) );
      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( h1_h2a_chi_fft_out.getData() ), h1_h2a_fft_in.getStorageArray().getData() ) );
      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( h1_h3a_chi_fft_out.getData() ), h1_h3a_fft_in.getStorageArray().getData() ) );
      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( h2_h2a_chi_fft_out.getData() ), h2_h2a_fft_in.getStorageArray().getData() ) );
      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( h2_h3a_chi_fft_out.getData() ), h2_h3a_fft_in.getStorageArray().getData() ) );
      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( h3_h3a_chi_fft_out.getData() ), h3_h3a_fft_in.getStorageArray().getData() ) );

      std::cout << "h1_h1a_fft_out: " << h1_h1a_fft_in.getStorageArray() << std::endl;


      // build m_pq fields
      auto m11_view = m11.getView();
      auto m12_view = m12.getView();
      auto m13_view = m13.getView();
      auto m22_view = m22.getView();
      auto m23_view = m23.getView();
      auto m33_view = m33.getView();

      auto const chi_view = chi.getConstView();

      auto const h1_h1a_fft_in_view = h1_h1a_fft_in.getConstView();
      auto const h1_h2a_fft_in_view = h1_h2a_fft_in.getConstView();
      auto const h1_h3a_fft_in_view = h1_h3a_fft_in.getConstView();
      auto const h2_h2a_fft_in_view = h2_h2a_fft_in.getConstView();
      auto const h2_h3a_fft_in_view = h2_h3a_fft_in.getConstView();
      auto const h3_h3a_fft_in_view = h3_h3a_fft_in.getConstView();

      auto set_domain = [=] __cuda_callable__ ( const IdxVect& idx ) mutable
      {
         const int i = idx.x();
         const int j = idx.y();

         const int chi_ij = chi_view( i, j );

         m11_view( i, j ) = chi_ij * h1_h1a_fft_in_view( i, j ) + 1 - ( chi_ij );
         m12_view( i, j ) = chi_ij * h1_h2a_fft_in_view( i, j );
         m13_view( i, j ) = chi_ij * h1_h3a_fft_in_view( i, j );

         m22_view( i, j ) = chi_ij * h2_h2a_fft_in_view( i, j ) + 1 - ( chi_ij );
         m23_view( i, j ) = chi_ij * h2_h3a_fft_in_view( i, j );
         m33_view( i, j ) = chi_ij * h3_h3a_fft_in_view( i, j ) + 1 - ( chi_ij );

      };
      IdxVect begin = { 0, 0 }, end = { gridDims.x(), gridDims.y() };
      TNL::Algorithms::parallelFor< Device >( begin, end, set_domain );

      //std::cout << "m11: "<< m11.getStorageArray() << std::endl;
      //std::cout << "m12: "<< m12.getStorageArray() << std::endl;
      //std::cout << "m13: "<< m13.getStorageArray() << std::endl;
      //std::cout << "m22: "<< m22.getStorageArray() << std::endl;
      //std::cout << "m23: "<< m23.getStorageArray() << std::endl;
      //std::cout << "m33: "<< m33.getStorageArray() << std::endl;

      // debug
      utils::dumpArrayToFile( m11, "results/field_m11.tnl" );
      utils::dumpArrayToFile( m12, "results/field_m12.tnl" );
      utils::dumpArrayToFile( m13, "results/field_m13.tnl" );
      utils::dumpArrayToFile( m12, "results/field_m21.tnl" );
      utils::dumpArrayToFile( m22, "results/field_m22.tnl" );
      utils::dumpArrayToFile( m23, "results/field_m23.tnl" );
      utils::dumpArrayToFile( m13, "results/field_m31.tnl" );
      utils::dumpArrayToFile( m23, "results/field_m32.tnl" );
      utils::dumpArrayToFile( m33, "results/field_m33.tnl" );

      //build static matrices and compute its inversions
      // TODO: This implementation of static matrices works only for 2x2, 3x3 and 4x4 matrices!
      using LocalMatrix = TNL::Matrices::StaticMatrix< RealType, 3, 3 >;

      auto b01_view = b01.getView();
      auto b02_view = b02.getView();
      auto b03_view = b03.getView();
      auto bx1_view = bx1.getView();
      auto bx2_view = bx2.getView();
      auto bx3_view = bx3.getView();
      auto by1_view = by1.getView();
      auto by2_view = by2.getView();
      auto by3_view = by3.getView();

      auto enforce_momentum_conditions = [=] __cuda_callable__ ( const IdxVect& idx ) mutable
      {
         const int i = idx.x();
         const int j = idx.y();

         LocalMatrix m;
         m( 0, 0 ) = m11( i, j ); m( 0, 1 ) = m12( i, j ); m( 0, 2 ) = m13( i, j );
         m( 1, 0 ) = m12( i, j ); m( 1, 1 ) = m22( i, j ); m( 1, 2 ) = m23( i, j );
         m( 2, 0 ) = m13( i, j ); m( 2, 1 ) = m23( i, j ); m( 2, 2 ) = m33( i, j );

         LocalMatrix inv_m = TNL::Matrices::inverse( m );
         //printf( "m11: %f, m12: %f, m13: %f, m21: %f, m22: %f, m23: %f, m31: %f, m32: %f, m33: %f \n", m( 0, 0 ), m( 0, 1 ), m( 0, 2 ), m( 1, 0 ), m( 1, 1 ), m( 1, 2 ), m( 2, 0 ), m( 2, 1 ), m( 2, 2 ) );

         b01_view( i, j ) = inv_m( 0, 0 );
         b02_view( i, j ) = inv_m( 0, 1 );
         b03_view( i, j ) = inv_m( 0, 2 );
         bx1_view( i, j ) = ( -1.f ) * inv_m( 1, 0 );
         bx2_view( i, j ) = ( -1.f ) * inv_m( 1, 1 );
         bx3_view( i, j ) = ( -1.f ) * inv_m( 1, 2 );
         by1_view( i, j ) = ( -1.f ) * inv_m( 2, 0 );
         by2_view( i, j ) = ( -1.f ) * inv_m( 2, 1 );
         by3_view( i, j ) = ( -1.f ) * inv_m( 2, 2 );
      };
      begin = { 0, 0 }, end = { gridDims.x(), gridDims.y() };
      TNL::Algorithms::parallelFor< Device >( begin, end, enforce_momentum_conditions );

      //std::cout << "b01: " << b01.getStorageArray() << std::endl;
      //std::cout << "b02: " << b01.getStorageArray() << std::endl;
      //std::cout << "b03: " << b01.getStorageArray() << std::endl;
      //std::cout << "bx1: " << b01.getStorageArray() << std::endl;
      //std::cout << "bx2: " << b01.getStorageArray() << std::endl;
      //std::cout << "bx3: " << b01.getStorageArray() << std::endl;
      //std::cout << "by1: " << b01.getStorageArray() << std::endl;
      //std::cout << "by2: " << b01.getStorageArray() << std::endl;
      //std::cout << "by3: " << b01.getStorageArray() << std::endl;

      // debug
      utils::dumpArrayToFile( b01, "results/field_b01.tnl");
      utils::dumpArrayToFile( b02, "results/field_b02.tnl");
      utils::dumpArrayToFile( b03, "results/field_b03.tnl");
      utils::dumpArrayToFile( bx1, "results/field_bx1.tnl");
      utils::dumpArrayToFile( bx2, "results/field_bx2.tnl");
      utils::dumpArrayToFile( bx3, "results/field_bx3.tnl");
      utils::dumpArrayToFile( by1, "results/field_by1.tnl");
      utils::dumpArrayToFile( by2, "results/field_by2.tnl");
      utils::dumpArrayToFile( by3, "results/field_by3.tnl");

   }

   // Algorithm 2
   template< typename NDArrayViewT >
   void
   evalInternalForces( NDArrayViewT inputField )
   {
      //NOTE: At this point, performace starts to be important, so the temporary arrays are part of the main classes
      utils::multiplyNDArrays( inputField.getView(), inputField.getView(), chi.getView() );

      //auto chi_linear_view = chi.getStorageArray().getView();
      //inputField.forAllElements(
      //      [=] __cuda_callable__ ( int i, RealType& value ) mutable
      //      {

      //      } );

      // perform the forward ffts:
      cufftHandle planr2c, planc2r;
      cudaStream_t stream = NULL;
      const int batch_size = 1; // NOTE: All these operations should be batched
      CUFFT_CALL( cufftCreate( &planr2c ) );
      CUFFT_CALL( cufftCreate( &planc2r ) );

      // d_chi_hat
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      // ax_cx1_ay_cy1_sum_hat, ax_cx2_ay_cy2_sum_hat, ax_cx3_ay_cy3_sum_hat
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );

      // D1, D2, D3
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );
      // B
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );

      CUDA_RT_CALL(cudaStreamCreateWithFlags( &stream, cudaStreamNonBlocking ) );
      CUFFT_CALL( cufftSetStream( planr2c, stream ) );
      CUFFT_CALL( cufftSetStream( planc2r, stream ) );

      // perform forward fft, obtain d_chi_hat
      //CUFFT_CALL( cufftExecR2C( planr2c, inputField.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( d_chi_hat.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, inputField.getData(), reinterpret_cast< cufftComplex* >( d_chi_hat.getData() ) ) );
      CUDA_RT_CALL(cudaStreamSynchronize(stream));

      // cufft doesn't include normalization
      const int pointsCount = gridDims.x() * gridDims.y();

      d_chi_hat_h1a_hat = d_chi_hat * h1a_hat / pointsCount;
      d_chi_hat_h2a_hat = d_chi_hat * h2a_hat / pointsCount;
      d_chi_hat_h3a_hat = d_chi_hat * h3a_hat / pointsCount;

      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( d_chi_hat_h1a_hat.getData() ), evalIntForce_D1.getStorageArray().getData() ) );
      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( d_chi_hat_h2a_hat.getData() ), evalIntForce_D2.getStorageArray().getData() ) );
      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( d_chi_hat_h3a_hat.getData() ), evalIntForce_D3.getStorageArray().getData() ) );
      CUDA_RT_CALL(cudaStreamSynchronize(stream));

      const auto bx1_view = bx1.getConstView();
      const auto bx2_view = bx2.getConstView();
      const auto bx3_view = bx3.getConstView();
      const auto by1_view = by1.getConstView();
      const auto by2_view = by2.getConstView();
      const auto by3_view = by3.getConstView();

      const auto evalIntForce_D1_view = evalIntForce_D1.getConstView();
      const auto evalIntForce_D2_view = evalIntForce_D2.getConstView();
      const auto evalIntForce_D3_view = evalIntForce_D3.getConstView();

      // this two fields are probably not necessary
      auto ax_view = ax.getView();
      auto ay_view = ay.getView();

      auto ax_cx1_ay_cy1_sum_view = ax_cx1_ay_cy1_sum.getView();
      auto ax_cx2_ay_cy2_sum_view = ax_cx2_ay_cy2_sum.getView();
      auto ax_cx3_ay_cy3_sum_view = ax_cx3_ay_cy3_sum.getView();

      const auto chi_view = chi.getConstView();
      const Real dV = stepSizes[ 0 ] * stepSizes[ 1 ];

      auto build_ax_ay = [=] __cuda_callable__ ( const IdxVect& idx ) mutable
      {
         const int i = idx.x();
         const int j = idx.y();

         const RealType bx1_ij = bx1_view( i, j );
         const RealType bx2_ij = bx2_view( i, j );
         const RealType bx3_ij = bx3_view( i, j );

         const RealType by1_ij = by1_view( i, j );
         const RealType by2_ij = by2_view( i, j );
         const RealType by3_ij = by3_view( i, j );

         const RealType ax_ij = bx1_ij * evalIntForce_D1_view( i, j ) + bx2_ij * evalIntForce_D2_view( i, j ) + bx3_ij * evalIntForce_D3_view( i, j );
         const RealType ay_ij = by1_ij * evalIntForce_D1_view( i, j ) + by2_ij * evalIntForce_D2_view( i, j ) + by3_ij * evalIntForce_D3_view( i, j );

         const RealType chi_ij = chi_view( i, j );

         const RealType cx1_ij = chi_ij * bx1_ij * dV;
         const RealType cx2_ij = chi_ij * bx2_ij * dV;
         const RealType cx3_ij = chi_ij * bx3_ij * dV;

         const RealType cy1_ij = chi_ij * by1_ij * dV;
         const RealType cy2_ij = chi_ij * by2_ij * dV;
         const RealType cy3_ij = chi_ij * by3_ij * dV;

         ax_cx1_ay_cy1_sum_view( i , j ) = cx1_ij * ax_ij + cy1_ij * ay_ij;
         ax_cx2_ay_cy2_sum_view( i , j ) = cx2_ij * ax_ij + cy2_ij * ay_ij;
         ax_cx3_ay_cy3_sum_view( i , j ) = cx3_ij * ax_ij + cy3_ij * ay_ij;
      };
      IdxVect begin = { 0, 0 }, end = { gridDims.x(), gridDims.y() };
      TNL::Algorithms::parallelFor< Device >( begin, end, build_ax_ay );

      CUFFT_CALL( cufftExecR2C( planr2c, ax_cx1_ay_cy1_sum.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( ax_cx1_ay_cy1_sum_hat.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, ax_cx2_ay_cy2_sum.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( ax_cx2_ay_cy2_sum_hat.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, ax_cx3_ay_cy3_sum.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( ax_cx3_ay_cy3_sum_hat.getData() ) ) );
      CUDA_RT_CALL( cudaStreamSynchronize( stream ) );

      // TODO: instead of b_hat, this could be summed up into ax_cx1_ay_cy1_sum array
      // cufft doesn't include normalization
      b_hat = ax_cx1_ay_cy1_sum_hat * h1a_bar_hat / pointsCount  + ax_cx2_ay_cy2_sum_hat * h2a_bar_hat / pointsCount +  ax_cx3_ay_cy3_sum_hat * h3a_bar_hat / pointsCount;

      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( b_hat.getData() ), f_int.getStorageArray().getData() ) );
      CUDA_RT_CALL(cudaStreamSynchronize(stream));

      // apply the characteristic function and boundary conditions
      const auto chi_g_view = chi_g.getConstView();
      auto f_int_view = f_int.getView();

      f_int.forAll(
            [=] __cuda_callable__ ( int i, int j ) mutable
            {
               f_int_view( i, j ) *= ( chi_view( i, j ) - chi_g_view( i, j ) );
            } );

      std::cout << "f_int: " << f_int.getStorageArray() << std::endl;

   }

   // Algorithm 4
   void
   evalExternalForces()
   {
      const int r2c_output_size = gridDims[ 0 ] * ( gridDims[ 1 ] / 2 + 1 );
      ArrayComplex b_f_ext_hat;
      b_f_ext_hat.setSize( r2c_output_size );

      NDArrayReal c01_b01;
      NDArrayReal c02_b02;
      NDArrayReal c03_b03;

      c01_b01.setSizes( gridDims.x(), gridDims.y() );
      c02_b02.setSizes( gridDims.x(), gridDims.y() );
      c03_b03.setSizes( gridDims.x(), gridDims.y() );

      ArrayComplex c01_b01_hat;
      ArrayComplex c02_b02_hat;
      ArrayComplex c03_b03_hat;

      c01_b01_hat.setSize( r2c_output_size );
      c02_b02_hat.setSize( r2c_output_size );
      c03_b03_hat.setSize( r2c_output_size );

      cufftHandle planr2c, planc2r;
      cudaStream_t stream = NULL;
      const int batch_size = 1; // NOTE: All these operations should be batched
      CUFFT_CALL( cufftCreate( &planr2c ) );
      CUFFT_CALL( cufftCreate( &planc2r ) );

      // cp01_b01_hat, cp02_b02_hat, cp03_b03_hat,
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      CUFFT_CALL( cufftPlanMany( &planr2c, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_R2C, batch_size ) );
      // B
      CUFFT_CALL( cufftPlanMany( &planc2r, gridDims.getSize(), gridDims.getData(), nullptr, 1, 0, nullptr, 1, 0, CUFFT_C2R, batch_size ) );

      CUDA_RT_CALL(cudaStreamCreateWithFlags( &stream, cudaStreamNonBlocking ) );
      CUFFT_CALL( cufftSetStream( planr2c, stream ) );
      CUFFT_CALL( cufftSetStream( planc2r, stream ) );

      const auto b01_view = b01.getConstView();
      const auto b02_view = b02.getConstView();
      const auto b03_view = b03.getConstView();

      const auto rhs_view = rhs.getConstView();

      auto c01_b01_view = c01_b01.getView();
      auto c02_b02_view = c02_b02.getView();
      auto c03_b03_view = c03_b03.getView();

      const auto chi_view = chi.getConstView();
      const Real dV = stepSizes[ 0 ] * stepSizes[ 1 ];

      auto build_cp0_r_products = [=] __cuda_callable__ ( const IdxVect& idx ) mutable
      {
         const int i = idx.x();
         const int j = idx.y();

         const RealType rhs_ij = rhs_view( i, j );
         const RealType chi_ij = chi_view( i, j );
         c01_b01_view( i, j ) = chi_ij * b01_view( i, j ) * dV * rhs_ij;
         c02_b02_view( i, j ) = chi_ij * b02_view( i, j ) * dV * rhs_ij;
         c03_b03_view( i, j ) = chi_ij * b03_view( i, j ) * dV * rhs_ij;
      };
      IdxVect begin = { 0, 0 }, end = { gridDims.x(), gridDims.y() };
      TNL::Algorithms::parallelFor< Device >( begin, end, build_cp0_r_products );

      CUFFT_CALL( cufftExecR2C( planr2c, c01_b01.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( c01_b01_hat.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, c02_b02.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( c02_b02_hat.getData() ) ) );
      CUFFT_CALL( cufftExecR2C( planr2c, c03_b03.getStorageArray().getData(), reinterpret_cast< cufftComplex* >( c03_b03_hat.getData() ) ) );

      // cufft doesn't include normalization
      const int pointsCount = gridDims.x() * gridDims.y();
      b_f_ext_hat = c01_b01_hat * h1a_bar_hat / pointsCount + c02_b02_hat * h2a_bar_hat / pointsCount + c03_b03_hat * h3a_bar_hat / pointsCount;

      CUFFT_CALL( cufftExecC2R( planc2r, reinterpret_cast< cufftComplex* >( b_f_ext_hat.getData() ), f_ext.getStorageArray().getData() ) );

      const auto chi_g_view = chi_g.getConstView();
      auto f_ext_view = f_ext.getView();

      std::cout << std::endl;
      std::cout << "f_ext: " << f_ext.getStorageArray() << std::endl;
      std::cout << std::endl;
      std::cout << "chi_g: " << chi_g.getStorageArray() << std::endl;
      std::cout << "chi: " << chi.getStorageArray() << std::endl;

      f_ext.forAll(
            [=] __cuda_callable__ ( int i, int j ) mutable
            {
               f_ext_view( i, j ) *= ( chi_view( i, j ) - chi_g_view( i, j ) );
            } );

      std::cout << std::endl;
      std::cout << "f_ext: " << f_ext.getStorageArray() << std::endl;
      std::cout << "f_ext: " << f_ext.getStorageArray() << std::endl;

      // debug
      utils::dumpArrayToFile( f_ext, "results/field_f_ext.tnl");
   }

   // Algorithm 3
   void
   evailField()
   {


   }

   //
   void
   solveWithConjugateGradients()
   {
      // - class related to linear solver
      // -- this should be part of the modified linear solver
      ArrayReal residue;
      ArrayReal p;
      NDArrayReal p_ndarray;
      ArrayReal f_int_p;

      RealType alpha;
      RealType beta;
      RealType residueNorm;
      RealType residueNormNew;

      RealType tolerance = 1.e-6;
      int maxInterations = 100;

      residue.setSize( gridDims.x() * gridDims.y() );
      p.setSize( gridDims.x() * gridDims.y() );
      p_ndarray.setSizes( gridDims.x(), gridDims.y() );

         std::cout << std::endl;
         std::cout << " f_int_p ZERO  data: " << f_int.getStorageArray() << std::endl;
         std::cout << std::endl;

      // initialize the solver
      f_int_vect = f_int.getStorageArray();
      f_ext_vect = f_ext.getStorageArray();
      residue = f_ext_vect - f_int_vect;
      residueNorm = std::pow( TNL::l2Norm( residue ), 2 );
      p = residue;
      std::cout << "Linear solver - initial residue: " << residue << std::endl;

      for( int i = 0; i < 50; i++ )
      {
         p_ndarray.getStorageArray() = p;
         evalInternalForces( p_ndarray.getView() );

         std::cout << std::endl;
         std::cout << "f_int_p: " << i << " data: " << f_int.getStorageArray() << std::endl;
         std::cout << std::endl;
         std::cout << "p_array:" << p << std::endl;
         std::cout << std::endl;
         // use linearized f_int_vect
         f_int_vect = f_int.getStorageArray();
         std::cout << "f_int_vect: " << f_int_vect << std::endl << std::endl;
         std::cout << "residueNorm: " << residueNorm << " product: " << ( p, f_int_vect ) << std::endl;

         alpha = residueNorm / ( p, f_int_vect );
         //No expression templates for NDArrays, no fun
         const auto p_view = p.getConstView();
         d.getStorageArray().forAllElements(
               [=] __cuda_callable__ ( int i, RealType& value ) mutable
               {
                  value += alpha * p_view[ i ];
               } );
         residue -= alpha * f_int_vect;
         std::cout << std::endl << "NewResidue: " << residue << std::endl;
         residueNormNew = std::pow( TNL::l2Norm( residue ), 2 );

         std::cout << "Step: " << i << ", new residue l2-norm: " << residueNormNew << ", alpha: " << alpha << std::endl;
         if( residueNormNew < tolerance ){
            std::cout << "Converged in " << i + 1 << " iterations." << std::endl;
            break;
         }

         beta = residueNormNew / residueNorm;
         p = residue + beta * p;
         residueNorm = residueNormNew;

      }

   }

//protected:

   // - class variables
   NDArrayReal u;
   //NDArrayReal d_hat;

   // - class domain specifier
   IdxVect gridDims = { 8, 8 };
   RealVect domainSizes = { 2.f, 2.f };
   RealVect domainExtensions = { 4.f / ( gridDims.x() - 2 ), 4.f / ( gridDims.y() - 2 ) };
   RealVect domainSizesWithExtensions = 0.;
   RealVect domainOrigin = { -1.f, -1.f };
   RealVect stepSizes = { ( domainSizes.x() + domainExtensions.x() ) / gridDims.x(), ( domainSizes.y() + domainExtensions.y() ) / gridDims.y() };

   // -- method parameters
   RealType kernelCoef = 1.5f;
   RealVect kernelWidth = { kernelCoef * stepSizes.x(), kernelCoef * stepSizes.y() };

   NDArrayReal x;
   NDArrayReal y;
   NDArrayReal chi;
   NDArrayReal chi_g;

   // - class base
   // -- this is basically only part of the initialization
   NDArrayReal h1;
   NDArrayReal h2;
   NDArrayReal h3;

   NDArrayReal h1a;
   NDArrayReal h2a;
   NDArrayReal h3a;

   NDArrayReal h1a_bar;
   NDArrayReal h2a_bar;
   NDArrayReal h3a_bar;

   // -- this is required by the simulation
   ArrayComplex h1a_hat;
   ArrayComplex h2a_hat;
   ArrayComplex h3a_hat;

   ArrayComplex h1a_bar_hat;
   ArrayComplex h2a_bar_hat;
   ArrayComplex h3a_bar_hat;

   NDArrayReal b01;
   NDArrayReal b02;
   NDArrayReal b03;

   NDArrayReal bx1;
   NDArrayReal bx2;
   NDArrayReal bx3;

   NDArrayReal by1;
   NDArrayReal by2;
   NDArrayReal by3;

   // - class related to evaluation of internal forces
   // -- this is required by eval force, uses temp arrays
   NDArrayReal d;
   ArrayComplex d_chi_hat;

   ArrayComplex d_chi_hat_h1a_hat;
   ArrayComplex d_chi_hat_h2a_hat;
   ArrayComplex d_chi_hat_h3a_hat;

   NDArrayReal evalIntForce_D1;
   NDArrayReal evalIntForce_D2;
   NDArrayReal evalIntForce_D3;

   NDArrayReal ax;
   NDArrayReal ay;

   NDArrayReal ax_cx1_ay_cy1_sum;
   NDArrayReal ax_cx2_ay_cy2_sum;
   NDArrayReal ax_cx3_ay_cy3_sum;

   ArrayComplex ax_cx1_ay_cy1_sum_hat;
   ArrayComplex ax_cx2_ay_cy2_sum_hat;
   ArrayComplex ax_cx3_ay_cy3_sum_hat;

   ArrayComplex b_hat;

   // TODO: Due simplicity in linear solver, I decided to use f_int as Vector. There are no expression-templates for the NDArray
   // and expression templates here key to make the linear solver simple.
   NDArrayReal f_int;
   ArrayReal f_int_vect;

   // - class related to evaluation of external forces
   // -- this is required by eval force, uses temp arrays
   NDArrayReal f_ext;
   ArrayReal f_ext_vect;

   NDArrayReal rhs;

   // -- function which defines the rhs
   __cuda_callable__
   RealType
   defineRhs( const RealType& x, const RealType& y )
   {
      return ( 1. ) * ( 4. - 2. * x * x - 2. * y * y );
   }




   // control

};

