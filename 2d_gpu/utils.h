namespace utils {

template< typename NDArrayT >
void fftshift( NDArrayT& array, int nx, int ny )
{
   using IdxVect = TNL::Containers::StaticVector< 2, int >;
   using DeviceType = typename NDArrayT::NDArrayStorage::DeviceType;

   if( ( nx % 2 != 0 ) || ( ny % 2 != 0 ) )
      std::cerr << "ERROR: Performing fftshift for array with odd size." << std::endl;

   const int nx_half = nx / 2;
   const int ny_half = ny / 2;

   auto array_view = array.getView();
   auto set_domain = [=] __cuda_callable__ ( const IdxVect& idx ) mutable
   {
      const int i = idx.x();
      const int j = idx.y();

      if( ( i < nx_half ) && ( j < ny_half ) )
         TNL::swap( array_view( i, j ), array_view( nx_half + i, ny_half + j ) );

      if( ( i >= nx_half ) && ( j < ny_half ) )
         TNL::swap( array_view( i, j ), array_view( i - ny_half, ny_half + j ) );
   };
   IdxVect begin = { 0, 0 }; IdxVect end = { nx, nx_half };
   TNL::Algorithms::parallelFor< DeviceType >( begin, end, set_domain );
}

template< typename NDArrayType >
void
dumpArrayToFile( NDArrayType& array, const std::string outputFilename )
{
   array.getStorageArray().save( outputFilename );
}

//TODO: Resolve how to do this, check expression templates
template< typename NDArrayViewType >
void
multiplyNDArrays( NDArrayViewType result_view, NDArrayViewType array_a_view, NDArrayViewType array_b_view )
{
   result_view.forAll(
         [=] __cuda_callable__ ( int i, int j ) mutable
         {
            result_view( i,  j ) = array_a_view( i, j ) * array_b_view( i, j );
         } );
}

} // namespace utils

